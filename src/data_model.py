# #############################################################################
# DATA (MODEL) PACKAGE CLASSES
# #############################################################################
import datetime
import os
import sqlite3
from sys import exc_info
from os import remove, sep
from time import strftime

from xlrd import xldate_as_tuple
from openpyxl import Workbook, load_workbook
from sqlite3 import connect


class TxtFileIO(object):
    """
    Model (data access object) for Input / Output Operations in txt files
    """

    # ========================================================================
    #                             Constructor
    # ========================================================================
    def __init__(self, filename):
        """
        None -> TxtFileIO

        filename - the complete filename path of the file

        Class constructor
        """
        self.filename = filename
        self.file = None  # _io.TextIOWrapper Object

    # ========================================================================
    #                            Public methods
    # ========================================================================
    def open(self, mode):
        """
        str -> _io.TextIOWrapper

        mode - 'r' for read or 'w' for write

        Opens a file in read or write mode
        """
        try:
            self.file = open(self.filename, mode, encoding='utf8')
        except IOError:
            e = exc_info()[1]
            print(e)

    def read(self, extension):
        """
        None -> list

        Return a List of lists each containing a line of the file
        """
        try:
            if extension == "txt":
                return [line.split('\t') for line in self.file]
            else:
                return [line.split(',') for line in self.file]
        except TypeError:
            e = exc_info()[1]
            print(e)

    def write(self, line_list):
        """
        list -> None

        line_list - a List of lists each containing the file lines

        Writes the line_list to the file
        """
        try:
            for line in line_list:
                self.file.write('\t'.join(word for word in line))
        except IOError:
            e = exc_info()[1]
            print(e)

    def write_line(self, line):
        """
        str -> None

        line - the string to write in the file

        Writes a line of text in a File
        """
        try:
            self.file.write(line)
        except IOError:
            e = exc_info()[1]
            print(e)

    def close(self):
        """
        None -> None

        Closes the stream and frees the file from memory
        """
        try:
            self.file.close()
        except IOError:
            e = exc_info()[1]
            print(e)

    def destroy(self):
        """
        None -> None

        Deletes the file
        """
        try:
            remove(self.filename)
        except IOError:
            e = exc_info()[1]
            print(e)


class ReadExcel(object):
    """
    Model (data access object) for Input Operations in xls files
    """

    # ========================================================================
    #                             Constructor
    # ========================================================================
    def __init__(self, log):
        """
        None -> ReadExcel

        log - LogManipulator object

        Class constructor
        """
        self.log = log
        self.workbook = None
        self.row_counter = -1
        self.worksheet = None
        self.num_rows = None
        self.num_cells = None

    # ========================================================================
    #                            Public methods
    # ========================================================================
    def open(self, file_name):
        """
        str -> list

        file_name - the complete path filename of the excel file

        Opens the Excel file and Returns a list containing the worksheet names
        """
        try:
            self.log.write("ReadExcel.open " +
                           file_name[file_name.rfind(sep) + 1:])
            self.workbook = load_workbook(file_name, data_only=True)
            return self.get_worksheet_names()
        except IOError:
            self.close()
            e = exc_info()[1]
            self.log.write('Erro! ReadExcel.open\n' + str(e))

    def decrease_row_counter(self):
        """
        None -> None

        Decreases by 1 the row counter
        """
        try:
            self.log.write("ReadExcel.decrease_row_counter " + "r_counter: " +
                           str(self.row_counter))
            self.row_counter -= 1
        except IOError:
            e = exc_info()[1]
            self.log.write('Erro! ReadExcel.decrease_row_counter\n' + str(e))

    def get_workbook(self):
        """
        None -> Workbook object

        Returns the Workbook object
        """
        try:
            self.log.write("ReadExcel.open_workbook " + str(self.workbook))
            return self.workbook
        except IOError:
            e = exc_info()[1]
            self.log.write('Erro! ReadExcel.get_workbook\n' + str(e))

    def get_worksheet_names(self):
        """
        None -> list

        Returns a list of workbook sheet names
        """
        try:
            self.log.write("ReadExcel.get_worksheet_names " +
                           str(self.workbook.sheetnames))
            return self.workbook.sheetnames  # sheet names
        except IOError:
            e = exc_info()[1]
            self.log.write('Erro! ReadExcel.get_worksheet_names\n' + str(e))

    def open_worksheet(self, worksheet_name):
        """
        str -> None

        Opens the Worksheet
        """
        try:
            self.row_counter = -1
            self.worksheet = self.workbook.get_sheet_by_name(worksheet_name)
            self.log.write("ReadExcel.open_worksheet " + worksheet_name)
            self.num_rows = self.worksheet.max_row - 1  # total number of rows
            self.num_cells = self.worksheet.max_column - 1  # total number of cols
        except IOError:
            e = exc_info()[1]
            self.log.write('Erro! ReadExcel.open_worksheet\n' + str(e))

    def get_n_rows(self):
        """
        None -> int

        Returns the number of rows
        """
        try:
            self.log.write("ReadExcel.get_n_rows - num_rows:" +
                           str(self.num_rows))
            return self.num_rows
        except IOError:
            e = exc_info()[1]
            self.log.write('Erro! ReadExcel.get_n_rows\n' + str(e))

    def get_next_row(self):
        """
        None -> list

        Returns a list containing the data from a Excel row
        """
        data_row = []
        if self.row_counter < 0:
            self.row_counter = 1
        else:
            self.row_counter += 1
        cell = 0

        try:
            while cell < self.num_cells + 1:
                cell += 1
                cell_value = self.worksheet.cell(self.row_counter, cell).value

                # Format cell values to append
                if type(cell_value) is None or type(cell_value) is 'NoneType':
                    data_row.append('')
                # In Excel all ints are floats .0 -----------------------------
                elif type(cell_value) is float and str(
                        cell_value)[-2:] == ".0" or str(cell_value)[-2:] == ",0":
                    data_row.append(int(cell_value))

                # datetime ----------------------------------------------------
                elif type(cell_value) is tuple:
                    data_row.append(xldate_as_tuple(abs(cell_value), 0).date)
                    
                elif type(cell_value) is datetime.time:
                    data_row.append(strftime("%H:%M:%S"))

                # integers ---------------------------------------------------
                elif type(cell_value) is int:
                    data_row.append(cell_value)

                # real floats --------------- ---------------------------------
                elif type(cell_value) is float:
                    data_row.append(cell_value)

                # empty string ------------------------------------------------
                elif type(cell_value) is str and len(cell_value) == 1:
                    data_row.append('')

                elif type(cell_value) is str:
                    data_row.append(cell_value)

                # Unicode -----------------------------------------------------
                else:
                    try:  # In some files all values are Unicode, so
                        if len(str(cell_value)) > 2:
                            if str(cell_value)[-2:] == ".0":
                                data_row.append(int(cell_value))
                            elif type(cell_value) is str:
                                data_row.append(cell_value)
                            elif type(cell_value) is int:
                                data_row.append(cell_value)
                            else:
                                data_row.append(float(cell_value))
                    except TypeError:
                        if cell_value:
                            data_row.append(cell_value)
                        else:
                            data_row.append('')

            #  self.log.write("ReadExel.get_next_line - line:" + str(data_row))
            #  commented because turns the program very slow
            return data_row
        except IOError:
            e = exc_info()[1]
            self.log.write('Erro! ReadExcel.get_next_row\n' + str(e))

    def close(self):
        """
        None -> None

        Closes the stream and frees the file from memory
        """
        try:
            self.log.write("ReadExcel.close")
            self.workbook.close()
        except IOError:
            e = exc_info()[1]
            self.log.write('Erro! ReadExcel.close\n' + str(e))


class WriteExcel(object):
    """
    Model (data access object) for Output Operations in xls files
    """

    # ========================================================================
    #                             Constructor
    # ========================================================================
    def __init__(self, log):
        """
        None -> WriteExcel

        Class constructor
        """
        self.log = log
        self.workbook = Workbook()
        self.worksheet = None
        self.row_counter = 1
        self.workbook_active = False

    # ========================================================================
    #                            Public methods
    # ========================================================================
    def save_workbook(self, file_name):
        """
        str -> None

        Saves the excel file in the same path as the original file
        """
        try:
            self.log.write('WriteExcel.save_workbook :' +
                           file_name[file_name.rfind(sep) + 1:])
            self.workbook.save(file_name)
        except IOError:
            e = exc_info()[1]
            self.log.write('Erro! WriteExcel.save_workbook\n' + str(e))

    def add_sheet(self, sheet_name):
        """
        str -> None

        sheet_name - the name of the Excel sheet (tab)

        Adds a sheet to the workbook
        """
        try:
            if not self.workbook_active:
                self.worksheet = self.workbook.active
                self.workbook_active = True
            else:
                self.worksheet = self.__create_sheet()
            try:
                self.worksheet.title = str(sheet_name)
            except ValueError:
                self.worksheet.title = 'Sem interesse'
            self.row_counter = 1
            self.log.write('WriteExcel.add_sheet :' + str(sheet_name))
        except IOError:
            e = exc_info()[1]
            self.log.write('Erro! WriteExcel.add_sheet\n' + str(e))

    def write_row(self, row_data, col=1):
        """
        list, int -> None

        row_data - A list containing the data to write in a excel row
        col - the starter column index

        Writes a row_data of excel
        """
        try:
            for i in range(len(row_data)):
                row = self.row_counter
                cell = self.worksheet.cell(row=row, column=i + col)
                cell.value = row_data[i]
            self.row_counter += 1
            #  self.log.write('WriteExcel.write_row: ' + str(row_data))
        except IOError:
            e = exc_info()[1]
            self.log.write('Erro! WriteExcel.write_row\n' + str(row_data) +
                           str(e))

    # ========================================================================
    #                            Private methods
    # ========================================================================
    def __create_sheet(self):
        """
        None -> None

        Returns a created worksheet
        """
        try:
            self.log.write('WriteExcel.__create_sheet')
            return self.workbook.create_sheet()
        except IOError:
            e = exc_info()[1]
            self.log.write('Erro! WriteExcel.__create_sheet\n' + str(e))


class CsvDB(object):
    """
    Model (data access object) for sqlite3 database manipulation from a csv
    """
    # ========================================================================
    #                             Constructor
    # ========================================================================
    def __init__(self, path, log, csvfile):
        """
        str, LogObject, str -> None

        path - the complete directory path where the database will be created
        log - LogManipulator object
        csvfile - the csv file to build the database from

        Class constructor
        """
        self.path = ''
        for char in path:
            if char == '/':
                self.path += '\\'
            else:
                self.path += char
        self.log = log
        self.csvfile = csvfile
        self.conn = None
        self.cur = None

    # ========================================================================
    #                            Public methods
    # ========================================================================
    def create(self):
        """
        None -> None

        Creates a sqlite database from a csv file
        """
        try:
            os.system(
                'csv-to-sqlite -f "' + self.path + self.csvfile + '" --find-types -v -o "' + self.path + 'out.sqlite"')
            self.conn = connect(self.path + 'out.sqlite')
            self.cur = self.conn.cursor()
            self.log.write("Base de dados construida com sucesso a partir do ficheiro csv")
        except IOError:
            e = exc_info()[1]
            self.log.write('Erro! CsvDB.create\nTente instalar csv-to-sqlite com pip install csv-to-sqlite' + '\n' +
                           str(e))

    def filter(self):
        """
        None -> List

        Removes null latitudes/longitudes and repeated lines from de database
        Returns a list with the filtered data
        """
        lines = [[]]
        _all = "SELECT COUNT(*) FROM '" + self.csvfile[:-4] + "';"
        try:
            self.cur.execute(_all)
            lines = self.cur.fetchall()
            self.log.write(str(lines[0][0]) + ' linhas inseridas na base de dados')
        except IOError:
            e = exc_info()[1]
            self.log.write('Erro! CsvDB.filter ao executar ' + _all + '\n' + str(e))

        del_null_lat_long = "DELETE FROM '" + self.csvfile[:-4] + "' WHERE Latitude == '' OR Latitude == '0';"

        try:
            self.cur.execute(del_null_lat_long)
            self.conn.commit()
            self.log.write("Latitudes e Longitudes nulas removidas da base de dados")
        except IOError:
            e = exc_info()[1]
            self.log.write('Erro! CsvDB.filter ao executar ' + del_null_lat_long + '\n' + str(e))
            self.conn.close()

        query = "SELECT * FROM '" + self.csvfile[:-4] + "'" + " GROUP BY Latitude, Longitude, Moving;"

        try:
            self.cur.execute(query)
            data = self.cur.fetchall()
            self.log.write("Linhas repetidas removidas da base de dados")
            self.log.write(str(len(data)) + " linhas válidas de dados")
            self.log.write(str(lines[0][0] - len(data)) + " linhas removidas da base de dados")
            self.conn.close()
            return data
        except IOError:
            e = exc_info()[1]
            self.log.write('Erro! CsvDB.filter ao executar ' + query + '\n' + str(e))
            self.conn.close()

    def destroy(self):
        """
        None -> None

        Deletes the database and the temporary csv files
        """
        try:            
            remove(self.path + 'out.sqlite')
            remove(self.path + self.csvfile)
            self.log.write('Ficheiros temporários removidos com sucesso')
        except IOError:
            e = exc_info()[1]
            self.log.write('Erro! TempDB.destroy\n' + str(e))


class TempDB(object):
    """
    Model (data access object) for sqlite3 database manipulation
    """

    # ========================================================================
    #                             Constructor
    # ========================================================================
    def __init__(self, path, log):
        """
        str -> TempDB

        path - the complete directory path where the database will be created
        log - LogManipulator object

        Class constructor
        """
        self.path = path + 'temp.db'
        self.log = log
        self.conn = connect(self.path)
        self.cur = self.conn.cursor()
        self.col_names = []

    # ========================================================================
    #                            Public methods
    # ========================================================================
    def create(self, col_names, col_value_types):
        """
        list, list -> None

        col_names - the column names for the database table
        col_value_types - the data types for the column values for the
                          database table

        Executes a 'CREATE TABLE' query
        """
        tmp = []
        for name in col_names:
            if '/' in name:
                name = name.replace('/', '_')
            tmp.append(name)

        self.col_names = tmp

        if len(col_names) > len(col_value_types):
            diff = len(col_names) - len(col_value_types)
            while diff > 0:
                col_value_types.append('TEXT')
                diff -= 1

        values = ", ".join(i[0] + " " + i[1] for i in
                           list(zip(self.col_names, col_value_types)))

        query = "CREATE TABLE t1 (" + values + ");"

        try:
            self.log.write('TempDB.create : ' + query)
            self.cur.execute("DROP TABLE IF EXISTS t1")
            self.cur.execute(query)
        except IOError:
            e = exc_info()[1]
            self.log.write('Erro! TempDB.create\nquery = ' + query + '\n' +
                           str(e))

    def insert(self, values):
        """
        list -> None

        values - the values to insert in the table's columns of the database

        Executes a 'INSERT INTO' query
        """
        tmp = []
        for val in values:
            if type(val) is str and '/' in val:
                val = val.replace('/', '_')
            tmp.append(val)

        query = "INSERT INTO t1('" + \
                "', '".join(str(n) for n in self.col_names) + "') " + \
                'VALUES ("' + '", "'.join(str(v) for v in tmp) + '");'
        try:
            #  self.log.write('TempDB.insert :' + query)
            self.cur.execute(query)
        except sqlite3.OperationalError:
            e = exc_info()[1]
            self.log.write('Erro! TempDB.insert\nquery = ' + query + '\n' +
                           str(e))

    def commit(self):
        """
        None -> None

        Commits the changes to the database
        """
        try:
            self.log.write('TempDB.commit')
            self.conn.commit()
        except IOError:
            e = exc_info()[1]
            self.log.write('Erro! TempDB.commit\n' + str(e))

    def select(self, user_query=None):
        """
        str -> None

        user_query - a SELECT query defined by user

        Executes a 'SELECT' query or a user defined 'SELECT' query
        """
        if user_query:
            query = user_query
        else:
            query = "SELECT (" + ", ".join(n for n in self.col_names) + \
                    "FROM t1"

        try:
            #  self.log.write('TempDB.select: ' + query)
            self.cur.execute(query)
        except IOError:
            e = exc_info()[1]
            self.log.write('Erro! TempDB.select\nquery = ' + query + '\n' +
                           str(e))

    def fetch_one(self):
        """
        None -> List

        Return a List containing one line of the SELECT query results
        """
        try:
            return self.cur.fetchone()
        except IOError:
            e = exc_info()[1]
            self.log.write('Erro! TempDB.fetch_one\n' + str(e))

    def close(self):
        """
        None -> None

        Closes the connection to the database
        """
        try:
            self.log.write('TempDB.close')
            self.conn.close()
        except IOError:
            e = exc_info()[1]
            self.log.write('Erro! TempDB.close\n' + str(e))

    def destroy(self):
        """
        None -> None

        Deletes the database file
        """
        try:
            self.log.write('TempDB.destroy')
            remove(self.path)
        except IOError:
            e = exc_info()[1]
            self.log.write('Erro! TempDB.destroy\n' + str(e))

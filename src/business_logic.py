# #############################################################################
#                 BUSINESS LOGIC (VIEW MODEL) PACKAGE CLASSES
# #############################################################################

import csv
import sys
from os import sep, path, listdir
from sys import exc_info
from datetime import datetime, date
from data_model import TxtFileIO, ReadExcel, TempDB, WriteExcel, CsvDB


class FileManipulator(object):
    """
    Main controller for files manipulation
    """

    # ========================================================================
    #                             Constructor
    # ========================================================================
    def __init__(self, filename):
        """
        str -> FileManipulator

        filename - the complete filename path

        Class constructor
        """
        self.filename = filename
        self.log = None

    # ========================================================================
    #                            Public methods
    # ========================================================================
    def set_log(self, log):
        """
        LogManipulator -> None

        log - LogManipulator object

        Sets the log file
        """
        self.log = log

    def set_output_filename(self):
        """
        None -> str

        Builds a complete filename path and returns it
        """
        try:
            path_filename, extension = path.splitext(self.filename)
            num = 00
            _dir = listdir(path_filename[:path_filename.rindex('/') + 1])
            filename = path_filename[path_filename.rindex('/') + 1:]
            out = filename + '_filtrado_' + '%02d' % num + extension

            while True:
                if out in _dir:
                    num += 1
                    out = filename + '_filtrado_' + '%02d' % num + extension
                else:
                    return path_filename[:path_filename.rindex('/') + 1] + out

        except IOError:
            e = exc_info()[1]
            self.log.write('Erro! FileManipulator.set_output_filename\n' +
                           str(e))


class LogManipulator(FileManipulator):
    """
    Controller for log files manipulation
    """

    # ========================================================================
    #                             Constructor
    # ========================================================================
    def __init__(self, filename):
        """
        str -> LogManipulator

        filename - the complete filename path

        Class constructor
        """
        super(LogManipulator, self).__init__(filename)
        self._log = TxtFileIO(self.filename)
        self.__clear()
        self.set_log(self)
        self.console = sys.stdout

    # ========================================================================
    #                            Public methods
    # ========================================================================
    def write(self, line):
        """
        str -> None

        line - A line of text

        Writes a line to the log file
        """
        self.__open()
        self._log.write_line(self.__get_time() + ' - ' + line + '\n')
        self.close()

    def close(self):
        """
        None -> None

        Closes the stream and frees the file from memory
        """
        self._log.close()

    # ========================================================================
    #                            Private methods
    # ========================================================================
    def __open(self):
        """
        None -> bool

        Opens a log file
        """
        self._log.open('a')

    def __get_time(self):
        """
        None -> str

        Returns a formatted str of datetime.now()
        """
        return datetime.strftime(datetime.now(), '%d/%m/%Y - %H:%M:%S.%f')

    def __clear(self):
        """
        None -> None

        Clears the previous log
        """
        self._log.open('w')
        self._log.close()


class CsvManipulator(FileManipulator):
    """
    Controller for csv files manipulation
    """

    # ========================================================================
    #                             Constructor
    # ========================================================================
    def __init__(self, filename, log):
        """
        str -> CsvManipulator

        filename - the complete filename path
        log - LogManipulator object

        Class constructor
        """
        super(CsvManipulator, self).__init__(filename)
        self.titles = []
        self.data = []
        self.total_data = []
        self.row_len = 0
        self.set_log(log)

    # ========================================================================
    #                            Public methods
    # ========================================================================
    def csv_to_line_list(self, first_row, delimiter):
        """
        Bool, str -> Bool

        first_row - boolean to populate titles
        delimiter - csv file delimiter ';' or ','

        Reads csvfile and populate titles and data
        """
        try:
            with open(self.filename) as csvfile:
                spamreader = csv.reader(csvfile, delimiter=delimiter)
                for row in spamreader:
                    if first_row:
                        self.titles = row
                        first_row = False
                    else:
                        self.data.append(row)
                        if len(row) > self.row_len:
                            self.row_len = len(row)

            self.log.write("Listas de títulos e dados criadas a partir do csv")

            return True
        except IOError:
            e = exc_info()[1]
            self.log.write('Erro! CsvManipulator.csv_to_line_list\n' + str(e))
            return False

    def clean_csv(self, suffix):
        """
        str -> None

        suffix - the suffix of the filename (_temp.csv or _filtered.csv)

        Corrects titles and removes blank lines
        """
        if suffix == "_temp.csv":
            try:
                self.__correct_titles()
                self.log.write('Lista de títulos corrigida com sucesso')
            except IOError:
                e = exc_info()[1]
                self.log.write('Erro! CsvManipulator.__correct_titles\n' + str(e))

        try:
            self.__populate_total_data()
            self.log.write('Lista de dados total (títulos e dados) do ficheiro ' + suffix + ' criada com sucesso')
        except IOError:
            e = exc_info()[1]
            self.log.write('Erro! CsvManipulator.__populate_total_data\n' + str(e))

        try:
            self.__create_csv(suffix)
            self.log.write('Ficheiro ' + suffix + ' criado com sucesso')
        except IOError:
            e = exc_info()[1]
            self.log.write('Erro! CsvManipulator.__create_csv\n' + str(e))

        try:
            self.__remove_blank_lines(suffix)
            self.log.write('Linhas em branco removidas com sucesso do ficheiro ' + suffix)
        except IOError:
            e = exc_info()[1]
            self.log.write('Erro! CsvManipulator.__remove_blank_lines\n' + str(e))

    def write_csv(self):
        """
        None -> str

        Writes a new csv file filtered with the help of an sqlite database
        """
        csvfile = self.filename[self.filename.rfind("/") + 1:self.filename.rfind('.')] + '_temp.csv'
        
        db = CsvDB(self.filename[:self.filename.rfind("/") + 1], self.log, csvfile)

        db.create()
        self.data = db.filter()
        if len(self.data) > 0:
            self.filename = self.filename[:self.filename.rfind('.') + 1]
            db.destroy()

        self.clean_csv("_filtered.csv")

        self.filename = self.filename[:self.filename.rfind('.')] + '_filtered.csv'

        return self.filename

    # ========================================================================
    #                            Private methods
    # ========================================================================
    def __correct_titles(self):
        """
        None -> None

        Corrects titles so it doesn't have blank or repeated values
        """
        if len(self.titles) != self.row_len:
            dif = self.row_len - len(self.titles)
            for num in range(dif):
                self.titles.append(num)

        # remove spaces from the titles because of DB
        tmp_titles = []
        for title in self.titles:
            tmp_title = ''
            for char in title:
                if char == ' ':
                    tmp_title += '_'
                else:
                    tmp_title += char
            tmp_titles.append(tmp_title)
        self.titles = tmp_titles

        index = len(self.titles) - 1
        while index > 0:
            if self.titles[index] == self.titles[index - 1] or self.titles[index] == '':
                self.titles[index] = self.titles[index] + str(index)
            index -= 1

    def __populate_total_data(self):
        """
        None -> None

        Populates total_data with corrected titles and data
        """
        self.total_data = []
        self.total_data.append(self.titles)
        for row in self.data:
            if len(row) < self.row_len:
                diff = self.row_len - len(row)
                for num in range(diff):
                    row.append('')
            self.total_data.append(row)

    def __create_csv(self, suffix):
        """
        None -> None

        Writes a new csv file with corrected titles
        """
        with open(self.filename[:self.filename.rfind('.')] + suffix, 'w') as csvfile:
            spamwriter = csv.writer(csvfile)
            spamwriter.writerows(self.total_data)

    def __remove_blank_lines(self, suffix):
        """
        None -> None

        Removes blank lines from the temp csv
        """
        with open(self.filename[:self.filename.rfind('.')] + suffix, 'r') as filehandle:
            lines = filehandle.readlines()

        with open(self.filename[:self.filename.rfind('.')] + suffix, 'w') as filehandle:
            lines = filter(lambda x: x.strip(), lines)
            filehandle.writelines(lines)


class TxtManipulator(FileManipulator):
    """
    Controller for txt files manipulation
    """

    # ========================================================================
    #                             Constructor
    # ========================================================================
    def __init__(self, filename, log):
        """
        str -> TxtManipulator

        filename - the complete filename path
        log - LogManipulator object

        Class constructor
        """
        super(TxtManipulator, self).__init__(filename)
        self.line_list = []
        self.set_log(log)

    # ========================================================================
    #                            Public methods
    # ========================================================================
    def txt_to_line_list(self, extension):
        """
        None -> bool

        Opens a file, reads it and create a list containing another lists for
        each lines of the file. Returns a boolean flag accordingly
        """
        f = TxtFileIO(self.filename)
        try:
            f.open('r')
            self.line_list = f.read(extension)
            f.close()

            return True

        except IOError:
            e = exc_info()[1]
            self.log.write('Erro! TxtManipulator.txt_to_line_list\n' + str(e))
            f.close()

            return False

    def get_line_list(self):
        """
        None -> list

        Returns a line of text from the file
        """
        try:
            return self.line_list
        except IOError:
            e = exc_info()[1]
            self.log.write('Erro! TxtManipulator.get_line_list\n' + str(e))

    def set_line_list(self, line_list):
        """
        list -> None

        line_list - a list containing a line of text

        Sets the line list
        """
        self.line_list = line_list

    def clean_txt(self, selection, extension):
        """
        list -> None

        selection - a list of ints, representing the selection of filters to
                    apply in the line_list.
                    0 - don't apply filter
                    1 - apply filter
        extension - file extension (txt or csv)

        Applies a filter to line list accordingly to the selection list
        """
        if selection[0]:
            self.__remove_near_coords_lines()
        if selection[1]:
            self.__remove_NaN_lines()
        if selection[2]:
            self.__remove_blank_lines(extension)
        if selection[3]:
            self.__remove_duplicated_lines()

    def write_txt(self, set_new_filename=True):
        """
        None -> None

        set_new_filename - a bool flag to build or not a new filename

        Opens a file and writes to it a list containing each lines in lists.
        Chooses an appropriate name for the file created.
        """
        f = None
        try:
            if set_new_filename:
                output_filename = self.set_output_filename()
                f = TxtFileIO(output_filename)
                f.open('w')
                f.write(self.line_list)
            else:
                output_filename = self.filename
                f = TxtFileIO(output_filename)
                f.open('w')
                for line in self.line_list:
                    f.write_line(line)
            return output_filename[output_filename.rfind(sep) + 1:]
        except IOError:
            e = exc_info()[1]
            self.log.write('Erro! TxtManipulator.write_txt\n' + str(e))
        finally:
            if f:
                f.close()

    # ========================================================================
    #                            Private methods
    # ========================================================================
    def __remove_blank_lines(self, extension):
        """
        None -> None

        Remove the lists containing blank lines from the line list
        """
        try:
            temp_list = self.line_list[:]
            if extension == "txt":
                self.line_list = [i for i in temp_list if i[0][0].isnumeric()]
            else:
                self.line_list = [i for i in temp_list if i[0][4].isnumeric()]
        except IOError:
            e = exc_info()[1]
            self.log.write('Erro! TxtManipulator.__remove_blank_lines\n' +
                           str(e))

    def __remove_duplicated_lines(self):  # TODO: tinha a ver com tempo
        """
        None -> None

        Remove the duplicated lists from the line list
        """
        try:
            temp_list = self.line_list[:]
            self.line_list = []
            last_item = []

            for item in temp_list:
                if item != last_item:
                    self.line_list.append(item)
                    last_item = item
        except IOError:
            e = exc_info()[1]
            self.log.write('Erro! TxtManipulator.__remove_duplicated_lines\n' +
                           str(e))

    def __remove_NaN_lines(self):
        """
        None -> None

        Remove the lists containing lines with the word 'NaN' or
        'NaN (Não é um número)' from the line list
        """
        try:
            temp = self.line_list[:]
            self.line_list = [i for i in temp if "NaN" not in i[0]]
            temp = self.line_list[:]
            temp2 = [i for i in temp if "NaN (Não é um número)" not in i[0]]
            self.line_list = temp2
        except IOError:
            e = exc_info()[1]
            self.log.write('Erro! TxtManipulator.__remove_NaN_lines\n' +
                           str(e))

    def __remove_near_coords_lines(self):  # TODO: test
        """
        None -> None

        Remove the lists containing close coordinates lines from the line list
        """
        try:
            stopped = False
            moved = False
            previous_longitude = 0.0
            previous_latitude = 0.0
            temp_list = self.line_list[:]
            self.line_list = []

            for item in temp_list:
                if len(item) > 2:
                    longitude = float(item[5])
                    latitude = float(item[6].rstrip("\r\n"))
                    dif_long = abs(longitude - previous_longitude)
                    dif_lat = abs(latitude - previous_latitude)
                    coords_sum = (dif_long + dif_lat) / 2.0
                    if item[2] == "moved via ":
                        if stopped is True:
                            self.line_list.append(item)
                            stopped = False
                            moved = True
                            previous_longitude = longitude
                            previous_latitude = latitude
                        elif stopped is False and moved is True and \
                                coords_sum > 0.001:
                            self.line_list.append(item)
                            previous_longitude = longitude
                            previous_latitude = latitude
                        else:
                            # (stopped and moved) = False
                            self.line_list.append(item)
                            moved = True
                            previous_longitude = longitude
                            previous_latitude = latitude
                    elif item[2] == "stopped ":
                        if stopped is False:
                            self.line_list.append(item)
                            stopped = True
                            moved = False
                            previous_longitude = longitude
                            previous_latitude = latitude
                        elif stopped is True and coords_sum > 0.001:
                            self.line_list.append(item)
                            moved = False
                            previous_longitude = longitude
                            previous_latitude = latitude
        except IOError:
            e = exc_info()[1]
            self.log.write('Erro! TxtManipulator.__remove_coords_lines\n' +
                           str(e))


class XlsManipulator(FileManipulator):
    """
    Controller for Excel files manipulation
    """

    # ========================================================================
    #                             Constructor
    # ========================================================================
    def __init__(self, filename, log):
        """
        str -> XlsManipulator

        filename - the complete filename path
        log - LogManipulator object

        Class constructor
        """
        super(XlsManipulator, self).__init__(filename)
        self.set_log(log)
        self.chosen_worksheet = ''
        self.file = None  # file Object
        self.db = None
        self.headers = []
        self.headers_types = []
        self.chosen_col_index = 0
        self.xls_line = []
        self.num_dates_in_line = 0

    # ========================================================================
    #                            Public methods
    # ========================================================================
    def read(self):
        """
        None -> list

        Instantiates a ReadExcel object, opens it and returns a list containing
        the tabs names
        """
        try:
            self.file = ReadExcel(self.log)
            return self.file.open(self.filename)
        except IOError:
            e = exc_info()[1]
            self.file.close()
            self.log.write('Erro! XlsManipulator.read\n' + str(e))

    def choose_worksheet(self, worksheet_name=None):
        """
        str -> list

        worksheet_name - the name of the selected worksheet

        Sets the selected worksheet name to manipulate that worksheet
        """
        try:
            if worksheet_name:
                self.chosen_worksheet = worksheet_name
            else:
                self.chosen_worksheet = 'Final GE'

            return self.__get_columns_headers()
        except IOError:
            e = exc_info()[1]
            self.file.close()
            self.log.write('Erro! XlsManipulator.choose_worksheet\n' + str(e))

    def choose_reference_col(self, chosen_col_index=0):
        """
        int -> None

        chosen_col_index - the column index to filter

        Sets the reference column index
        """
        try:
            self.xls_line = self.file.get_next_row()
            if chosen_col_index:
                self.chosen_col_index = chosen_col_index
            else:
                current_index = -1
                for n in self.xls_line:
                    current_index += 1
                    if type(n) is tuple:
                        try:
                            # https://bytes.com/topic/python/answers/101490-create-datetime-instance-using-tuple
                            datetime(*n)
                            self.chosen_col_index = current_index
                        except IOError:
                            e = exc_info()[1]
                            self.file.close()
                            m1 = 'XlsManipulator.choose_reference_col\n'
                            m2 = 'Erro! Excel sem células de Datas\n' + str(e)
                            self.log.write('Erro!' + m1 + m2)
            if self.__format_datetime(False):
                self.__set_headers_types()
        except IOError:
            e = exc_info()[1]
            self.file.close()
            self.log.write('Erro! XlsManipulator.choose_reference_col\n' +
                           str(e))

    def xls_to_db(self):
        """
        None -> None

        Instantiates a sqlite database and fills it with every row of the
        selected worksheet
        """
        try:
            self.db = TempDB(self.filename[:self.filename.rindex('/') + 1],
                             self.log)
            self.db.create(self.headers, self.headers_types)
            self.db.insert(self.xls_line)
            n_rows = self.file.get_n_rows() - 2
            while n_rows:
                if self.__format_datetime():
                    self.db.insert(self.xls_line)
                n_rows -= 1
            self.db.commit()
        except IOError:
            self.db.close()
            e = exc_info()[1]
            m = 'XlsManipulator.xls_to_db\nErro ao inserir dados na DB\n'
            self.log.write(m + str(e))
        finally:
            self.file.close()

    def write(self, is_date_col=True):
        """
        None -> None

        Writes a filtered by date Excel file
        """
        try:
            formatted_xls = WriteExcel(self.log)

            if is_date_col:
                self.db.select("SELECT DISTINCT Data FROM t1")
                dates = []
                date_exists = True

                while date_exists:
                    _date = self.db.fetch_one()
                    if _date:
                        dates.append(_date)
                    else:
                        date_exists = False

                for _date in dates:
                    # _datetime_object = datetime.strptime(_date[0], '%d/%m/%Y')
                    _datetime_object = date.fromisoformat(_date[0][:10])
                    formatted_xls.add_sheet(self.__get_week_days(_datetime_object))
                    formatted_xls.write_row(self.headers)

                    query = "SELECT * FROM t1 WHERE Data = '" + _date[0][:10] + "'"
                    self.db.select(query)
                    row_exists = True

                    while row_exists:
                        row_values = self.db.fetch_one()

                        if row_values:
                            formatted_xls.write_row(row_values)
                        else:
                            row_exists = False
            else:
                col_name = self.headers[self.chosen_col_index]
                self.db.select(f"SELECT DISTINCT {col_name} FROM t1")
                values = []
                value_exists = True

                while value_exists:
                    _value = self.db.fetch_one()
                    if _value:
                        values.append(_value)
                    else:
                        value_exists = False

                for val in values:
                    formatted_xls.add_sheet(str(val[0]))
                    formatted_xls.write_row(self.headers)

                    query = f"SELECT * FROM t1 WHERE {col_name} = '" + str(val[0]) + "'"
                    self.db.select(query)
                    row_exists = True

                    while row_exists:
                        row_values = self.db.fetch_one()

                        if row_values:
                            formatted_xls.write_row(row_values)
                        else:
                            row_exists = False

            output_filename = self.set_output_filename()
            formatted_xls.save_workbook(output_filename)

            self.db.close()
            self.db.destroy()

            return output_filename[output_filename.rfind(sep) + 1:]

        except IOError:
            e = exc_info()[1]
            self.log.write('Erro! XlsManipulator.write\n' + str(e))

    # ========================================================================
    #                            Private methods
    # ========================================================================
    def __get_columns_headers(self):
        """
        None -> list

        Returns a list of the values in the first column of the chosen
        worksheet
        """
        try:
            self.file.open_worksheet(self.chosen_worksheet)
            self.headers = self.file.get_next_row()
            num = 1
            i = 0
            last_header = ''
            for header in self.headers:
                if not header:
                    new_header = last_header + str(num)
                    num += 1
                    self.headers[i] = new_header
                else:
                    last_header = header
                i += 1

            return self.headers
        except IOError:
            e = exc_info()[1]
            self.file.close()
            self.log.write('Erro! XlsManipulator.get_columns_headers\n' +
                           str(e))

    def __set_headers_types(self):
        """
        None -> None

        Creates a list of sqlite3 data types from the corresponding Excel data
        types
        """
        index = -1
        try:
            for n in self.xls_line:
                index += 1
                if type(n) is float and str(n)[-2:] != '.0':
                    self.headers_types.append('REAL')
                if type(n) is int or type(n) is float and str(n)[-2:] == '.0':
                    self.headers_types.append('INTEGER')
                if type(n) is str:
                    self.headers_types.append('TEXT')
                if type(n) is tuple or isinstance(n, datetime):
                    try:
                        datetime(*n)
                        self.num_dates_in_line += 1
                        self.headers_types.append('DATE')
                    except TypeError:
                        self.headers_types.append('BLOB')
                # else:
                #    self.headers_types.append('TEXT')
        except IOError:
            e = exc_info()[1]
            self.log.write('Erro! XlsManipulator.__set_headers_types\n' +
                           str(e))

    def __format_datetime(self, get_next_row=True):
        """
        bool -> bool

        get_next_row - boolean flag to get another row from the chosen Excel
                       worksheet

        Turns a datetime object into str and returns accordingly
        """
        try:
            description_index = self.headers.index('Description')
            if get_next_row:
                self.xls_line = self.file.get_next_row()
            try:
                if isinstance(self.xls_line[description_index], datetime):
                    _datetime_1 = self.xls_line[description_index]
                    _datetime_2 = self.xls_line[self.chosen_col_index]
                    str_time = _datetime_1.strftime("%H:%M:%S")
                    str_date = _datetime_2.strftime("%d/%m/%Y")
                    self.xls_line[description_index] = str_time
                    self.xls_line[self.chosen_col_index] = self.__get_week_days(_datetime_2)

                return True
            except IOError:
                return False
        except IOError:
            e = exc_info()[1]
            self.log.write('Erro! XlsManipulator.__format_datetime\n' + str(e))

    def __close_file(self):
        """
        None -> None

        Closes the stream and frees the file from memory
        """
        try:
            self.file.close()
        except IOError:
            e = exc_info()[1]
            self.log.write('Erro! XlsManipulator.__close_file\n' + str(e))

    def __get_week_days(self, _date):
        """
        datetime -> str

        _date - datetime object

        Returns a str representation of the date to be the name of the Excel
        Separator
        """
        week_day = ['2_', '3_', '4_', '5_', '6_', 'S_', 'D_']
        m = {1: 'JAN', 2: 'FEB', 3: 'MAR', 4: 'ABR', 5: 'MAI', 6: 'JUN',
             7: 'JUL', 8: 'AGO', 9: 'SET', 10: 'OUT', 11: 'NOV', 12: 'DEC'}
        '''
        date.weekday()
        Return the day of the week as an integer, where Monday is 0
        and Sunday is 6.
        For example, date(2002, 12, 4).weekday() == 2, a Wednesday
        '''
        try:
            return week_day[_date.weekday()] + str(_date.day) + m[_date.month]
        except IOError:
            e = exc_info()[1]
            self.log.write('Erro! XlsManipulator.__get_week_days\n' + str(e))

# #############################################################################
# IMPORTS
# #############################################################################

import os
import csv
import sqlite3
import datetime
import sys

from sys import exc_info, platform, stdout
from os import remove, sep, path, listdir
from codecs import getwriter
from datetime import datetime, date
from time import strftime
from sqlite3 import connect
from tkinter import Tk, filedialog, Label, Button, Frame, Checkbutton, Canvas,\
    Menu, Toplevel, Radiobutton, IntVar, StringVar, W, E, NW, EW, FALSE
if platform == 'linux':
    from os import O_RDONLY, open as o_open
else:
    from os import startfile

from xlrd import xldate_as_tuple
from openpyxl import Workbook, load_workbook


# #############################################################################
# DATA (MODEL) PACKAGE CLASSES
# #############################################################################

class TxtFileIO(object):
    """
    Model (data access object) for Input / Output Operations in txt files
    """

    # ========================================================================
    #                             Constructor
    # ========================================================================
    def __init__(self, filename):
        """
        None -> TxtFileIO

        filename - the complete filename path of the file

        Class constructor
        """
        self.filename = filename
        self.file = None  # _io.TextIOWrapper Object

    # ========================================================================
    #                            Public methods
    # ========================================================================
    def open(self, mode):
        """
        str -> _io.TextIOWrapper

        mode - 'r' for read or 'w' for write

        Opens a file in read or write mode
        """
        try:
            self.file = open(self.filename, mode, encoding='utf8')
        except IOError:
            e = exc_info()[1]
            print(e)

    def read(self, extension):
        """
        None -> list

        Return a List of lists each containing a line of the file
        """
        try:
            if extension == "txt":
                return [line.split('\t') for line in self.file]
            else:
                return [line.split(',') for line in self.file]
        except TypeError:
            e = exc_info()[1]
            print(e)

    def write(self, line_list):
        """
        list -> None

        line_list - a List of lists each containing the file lines

        Writes the line_list to the file
        """
        try:
            for line in line_list:
                self.file.write('\t'.join(word for word in line))
        except IOError:
            e = exc_info()[1]
            print(e)

    def write_line(self, line):
        """
        str -> None

        line - the string to write in the file

        Writes a line of text in a File
        """
        try:
            self.file.write(line)
        except IOError:
            e = exc_info()[1]
            print(e)

    def close(self):
        """
        None -> None

        Closes the stream and frees the file from memory
        """
        try:
            self.file.close()
        except IOError:
            e = exc_info()[1]
            print(e)

    def destroy(self):
        """
        None -> None

        Deletes the file
        """
        try:
            remove(self.filename)
        except IOError:
            e = exc_info()[1]
            print(e)


class ReadExcel(object):
    """
    Model (data access object) for Input Operations in xls files
    """

    # ========================================================================
    #                             Constructor
    # ========================================================================
    def __init__(self, log):
        """
        None -> ReadExcel

        log - LogManipulator object

        Class constructor
        """
        self.log = log
        self.workbook = None
        self.row_counter = -1
        self.worksheet = None
        self.num_rows = None
        self.num_cells = None

    # ========================================================================
    #                            Public methods
    # ========================================================================
    def open(self, file_name):
        """
        str -> list

        file_name - the complete path filename of the excel file

        Opens the Excel file and Returns a list containing the worksheet names
        """
        try:
            self.log.write("ReadExcel.open " +
                           file_name[file_name.rfind(sep) + 1:])
            self.workbook = load_workbook(file_name, data_only=True)
            return self.get_worksheet_names()
        except IOError:
            self.close()
            e = exc_info()[1]
            self.log.write('Erro! ReadExcel.open\n' + str(e))

    def decrease_row_counter(self):
        """
        None -> None

        Decreases by 1 the row counter
        """
        try:
            self.log.write("ReadExcel.decrease_row_counter " + "r_counter: " +
                           str(self.row_counter))
            self.row_counter -= 1
        except IOError:
            e = exc_info()[1]
            self.log.write('Erro! ReadExcel.decrease_row_counter\n' + str(e))

    def get_workbook(self):
        """
        None -> Workbook object

        Returns the Workbook object
        """
        try:
            self.log.write("ReadExcel.open_workbook " + str(self.workbook))
            return self.workbook
        except IOError:
            e = exc_info()[1]
            self.log.write('Erro! ReadExcel.get_workbook\n' + str(e))

    def get_worksheet_names(self):
        """
        None -> list

        Returns a list of workbook sheet names
        """
        try:
            self.log.write("ReadExcel.get_worksheet_names " +
                           str(self.workbook.sheetnames))
            return self.workbook.sheetnames  # sheet names
        except IOError:
            e = exc_info()[1]
            self.log.write('Erro! ReadExcel.get_worksheet_names\n' + str(e))

    def open_worksheet(self, worksheet_name):
        """
        str -> None

        Opens the Worksheet
        """
        try:
            self.row_counter = -1
            self.worksheet = self.workbook.get_sheet_by_name(worksheet_name)
            self.log.write("ReadExcel.open_worksheet " + worksheet_name)
            self.num_rows = self.worksheet.max_row - 1  # total number of rows
            self.num_cells = self.worksheet.max_column - 1  # total number of cols
        except IOError:
            e = exc_info()[1]
            self.log.write('Erro! ReadExcel.open_worksheet\n' + str(e))

    def get_n_rows(self):
        """
        None -> int

        Returns the number of rows
        """
        try:
            self.log.write("ReadExcel.get_n_rows - num_rows:" +
                           str(self.num_rows))
            return self.num_rows
        except IOError:
            e = exc_info()[1]
            self.log.write('Erro! ReadExcel.get_n_rows\n' + str(e))

    def get_next_row(self):
        """
        None -> list

        Returns a list containing the data from a Excel row
        """
        data_row = []
        if self.row_counter < 0:
            self.row_counter = 1
        else:
            self.row_counter += 1
        cell = 0

        try:
            while cell < self.num_cells + 1:
                cell += 1
                cell_value = self.worksheet.cell(self.row_counter, cell).value

                # Format cell values to append
                if type(cell_value) is None or type(cell_value) is 'NoneType':
                    data_row.append('')
                # In Excel all ints are floats .0 -----------------------------
                elif type(cell_value) is float and str(
                        cell_value)[-2:] == ".0" or str(cell_value)[-2:] == ",0":
                    data_row.append(int(cell_value))

                # datetime ----------------------------------------------------
                elif type(cell_value) is tuple:
                    data_row.append(xldate_as_tuple(abs(cell_value), 0).date)

                elif type(cell_value) is datetime.time:
                    data_row.append(strftime("%H:%M:%S"))

                # integers ---------------------------------------------------
                elif type(cell_value) is int:
                    data_row.append(cell_value)

                # real floats --------------- ---------------------------------
                elif type(cell_value) is float:
                    data_row.append(cell_value)

                # empty string ------------------------------------------------
                elif type(cell_value) is str and len(cell_value) == 1:
                    data_row.append('')

                elif type(cell_value) is str:
                    data_row.append(cell_value)

                # Unicode -----------------------------------------------------
                else:
                    try:  # In some files all values are Unicode, so
                        if len(str(cell_value)) > 2:
                            if str(cell_value)[-2:] == ".0":
                                data_row.append(int(cell_value))
                            elif type(cell_value) is str:
                                data_row.append(cell_value)
                            elif type(cell_value) is int:
                                data_row.append(cell_value)
                            else:
                                data_row.append(float(cell_value))
                    except TypeError:
                        if cell_value:
                            data_row.append(cell_value)
                        else:
                            data_row.append('')

            #  self.log.write("ReadExel.get_next_line - line:" + str(data_row))
            #  commented because turns the program very slow
            return data_row
        except IOError:
            e = exc_info()[1]
            self.log.write('Erro! ReadExcel.get_next_row\n' + str(e))

    def close(self):
        """
        None -> None

        Closes the stream and frees the file from memory
        """
        try:
            self.log.write("ReadExcel.close")
            self.workbook.close()
        except IOError:
            e = exc_info()[1]
            self.log.write('Erro! ReadExcel.close\n' + str(e))


class WriteExcel(object):
    """
    Model (data access object) for Output Operations in xls files
    """

    # ========================================================================
    #                             Constructor
    # ========================================================================
    def __init__(self, log):
        """
        None -> WriteExcel

        Class constructor
        """
        self.log = log
        self.workbook = Workbook()
        self.worksheet = None
        self.row_counter = 1
        self.workbook_active = False

    # ========================================================================
    #                            Public methods
    # ========================================================================
    def save_workbook(self, file_name):
        """
        str -> None

        Saves the excel file in the same path as the original file
        """
        try:
            self.log.write('WriteExcel.save_workbook :' +
                           file_name[file_name.rfind(sep) + 1:])
            self.workbook.save(file_name)
        except IOError:
            e = exc_info()[1]
            self.log.write('Erro! WriteExcel.save_workbook\n' + str(e))

    def add_sheet(self, sheet_name):
        """
        str -> None

        sheet_name - the name of the Excel sheet (tab)

        Adds a sheet to the workbook
        """
        try:
            if not self.workbook_active:
                self.worksheet = self.workbook.active
                self.workbook_active = True
            else:
                self.worksheet = self.__create_sheet()
            try:
                self.worksheet.title = str(sheet_name)
            except ValueError:
                self.worksheet.title = 'Sem interesse'
            self.row_counter = 1
            self.log.write('WriteExcel.add_sheet :' + str(sheet_name))
        except IOError:
            e = exc_info()[1]
            self.log.write('Erro! WriteExcel.add_sheet\n' + str(e))

    def write_row(self, row_data, col=1):
        """
        list, int -> None

        row_data - A list containing the data to write in a excel row
        col - the starter column index

        Writes a row_data of excel
        """
        try:
            for i in range(len(row_data)):
                row = self.row_counter
                cell = self.worksheet.cell(row=row, column=i + col)
                cell.value = row_data[i]
            self.row_counter += 1
            #  self.log.write('WriteExcel.write_row: ' + str(row_data))
        except IOError:
            e = exc_info()[1]
            self.log.write('Erro! WriteExcel.write_row\n' + str(row_data) +
                           str(e))

    # ========================================================================
    #                            Private methods
    # ========================================================================
    def __create_sheet(self):
        """
        None -> None

        Returns a created worksheet
        """
        try:
            self.log.write('WriteExcel.__create_sheet')
            return self.workbook.create_sheet()
        except IOError:
            e = exc_info()[1]
            self.log.write('Erro! WriteExcel.__create_sheet\n' + str(e))


class CsvDB(object):
    """
    Model (data access object) for sqlite3 database manipulation from a csv
    """

    # ========================================================================
    #                             Constructor
    # ========================================================================
    def __init__(self, path, log, csvfile):
        """
        str, LogObject, str -> None

        path - the complete directory path where the database will be created
        log - LogManipulator object
        csvfile - the csv file to build the database from

        Class constructor
        """
        self.path = ''
        for char in path:
            if char == '/':
                self.path += '\\'
            else:
                self.path += char
        self.log = log
        self.csvfile = csvfile
        self.conn = None
        self.cur = None

    # ========================================================================
    #                            Public methods
    # ========================================================================
    def create(self):
        """
        None -> None

        Creates a sqlite database from a csv file
        """
        try:
            os.system(
                'csv-to-sqlite -f "' + self.path + self.csvfile + '" --find-types -v -o "' + self.path + 'out.sqlite"')
            self.conn = connect(self.path + 'out.sqlite')
            self.cur = self.conn.cursor()
            self.log.write("Base de dados construida com sucesso a partir do ficheiro csv")
        except IOError:
            e = exc_info()[1]
            self.log.write('Erro! CsvDB.create\nTente instalar csv-to-sqlite com pip install csv-to-sqlite' + '\n' +
                           str(e))

    def filter(self):
        """
        None -> List

        Removes null latitudes/longitudes and repeated lines from de database
        Returns a list with the filtered data
        """
        lines = [[]]
        _all = "SELECT COUNT(*) FROM '" + self.csvfile[:-4] + "';"
        try:
            self.cur.execute(_all)
            lines = self.cur.fetchall()
            self.log.write(str(lines[0][0]) + ' linhas inseridas na base de dados')
        except IOError:
            e = exc_info()[1]
            self.log.write('Erro! CsvDB.filter ao executar ' + _all + '\n' + str(e))

        del_null_lat_long = "DELETE FROM '" + self.csvfile[:-4] + "' WHERE Latitude == '' OR Latitude == '0';"

        try:
            self.cur.execute(del_null_lat_long)
            self.conn.commit()
            self.log.write("Latitudes e Longitudes nulas removidas da base de dados")
        except IOError:
            e = exc_info()[1]
            self.log.write('Erro! CsvDB.filter ao executar ' + del_null_lat_long + '\n' + str(e))
            self.conn.close()

        query = "SELECT * FROM '" + self.csvfile[:-4] + "'" + " GROUP BY Latitude, Longitude, Moving;"

        try:
            self.cur.execute(query)
            data = self.cur.fetchall()
            self.log.write("Linhas repetidas removidas da base de dados")
            self.log.write(str(len(data)) + " linhas válidas de dados")
            self.log.write(str(lines[0][0] - len(data)) + " linhas removidas da base de dados")
            self.conn.close()
            return data
        except IOError:
            e = exc_info()[1]
            self.log.write('Erro! CsvDB.filter ao executar ' + query + '\n' + str(e))
            self.conn.close()

    def destroy(self):
        """
        None -> None

        Deletes the database and the temporary csv files
        """
        try:
            remove(self.path + 'out.sqlite')
            remove(self.path + self.csvfile)
            self.log.write('Ficheiros temporários removidos com sucesso')
        except IOError:
            e = exc_info()[1]
            self.log.write('Erro! TempDB.destroy\n' + str(e))


class TempDB(object):
    """
    Model (data access object) for sqlite3 database manipulation
    """

    # ========================================================================
    #                             Constructor
    # ========================================================================
    def __init__(self, path, log):
        """
        str -> TempDB

        path - the complete directory path where the database will be created
        log - LogManipulator object

        Class constructor
        """
        self.path = path + 'temp.db'
        self.log = log
        self.conn = connect(self.path)
        self.cur = self.conn.cursor()
        self.col_names = []

    # ========================================================================
    #                            Public methods
    # ========================================================================
    def create(self, col_names, col_value_types):
        """
        list, list -> None

        col_names - the column names for the database table
        col_value_types - the data types for the column values for the
                          database table

        Executes a 'CREATE TABLE' query
        """
        tmp = []
        for name in col_names:
            if '/' in name:
                name = name.replace('/', '_')
            tmp.append(name)

        self.col_names = tmp

        if len(col_names) > len(col_value_types):
            diff = len(col_names) - len(col_value_types)
            while diff > 0:
                col_value_types.append('TEXT')
                diff -= 1

        values = ", ".join(i[0] + " " + i[1] for i in
                           list(zip(self.col_names, col_value_types)))

        query = "CREATE TABLE t1 (" + values + ");"

        try:
            self.log.write('TempDB.create : ' + query)
            self.cur.execute("DROP TABLE IF EXISTS t1")
            self.cur.execute(query)
        except IOError:
            e = exc_info()[1]
            self.log.write('Erro! TempDB.create\nquery = ' + query + '\n' +
                           str(e))

    def insert(self, values):
        """
        list -> None

        values - the values to insert in the table's columns of the database

        Executes a 'INSERT INTO' query
        """
        tmp = []
        for val in values:
            if type(val) is str and '/' in val:
                val = val.replace('/', '_')
            tmp.append(val)

        query = "INSERT INTO t1('" + \
                "', '".join(str(n) for n in self.col_names) + "') " + \
                'VALUES ("' + '", "'.join(str(v) for v in tmp) + '");'
        try:
            #  self.log.write('TempDB.insert :' + query)
            self.cur.execute(query)
        except sqlite3.OperationalError:
            e = exc_info()[1]
            self.log.write('Erro! TempDB.insert\nquery = ' + query + '\n' +
                           str(e))

    def commit(self):
        """
        None -> None

        Commits the changes to the database
        """
        try:
            self.log.write('TempDB.commit')
            self.conn.commit()
        except IOError:
            e = exc_info()[1]
            self.log.write('Erro! TempDB.commit\n' + str(e))

    def select(self, user_query=None):
        """
        str -> None

        user_query - a SELECT query defined by user

        Executes a 'SELECT' query or a user defined 'SELECT' query
        """
        if user_query:
            query = user_query
        else:
            query = "SELECT (" + ", ".join(n for n in self.col_names) + \
                    "FROM t1"

        try:
            #  self.log.write('TempDB.select: ' + query)
            self.cur.execute(query)
        except IOError:
            e = exc_info()[1]
            self.log.write('Erro! TempDB.select\nquery = ' + query + '\n' +
                           str(e))

    def fetch_one(self):
        """
        None -> List

        Return a List containing one line of the SELECT query results
        """
        try:
            return self.cur.fetchone()
        except IOError:
            e = exc_info()[1]
            self.log.write('Erro! TempDB.fetch_one\n' + str(e))

    def close(self):
        """
        None -> None

        Closes the connection to the database
        """
        try:
            self.log.write('TempDB.close')
            self.conn.close()
        except IOError:
            e = exc_info()[1]
            self.log.write('Erro! TempDB.close\n' + str(e))

    def destroy(self):
        """
        None -> None

        Deletes the database file
        """
        try:
            self.log.write('TempDB.destroy')
            remove(self.path)
        except IOError:
            e = exc_info()[1]
            self.log.write('Erro! TempDB.destroy\n' + str(e))


# #############################################################################
#                 BUSINESS LOGIC (VIEW MODEL) PACKAGE CLASSES
# #############################################################################

class FileManipulator(object):
    """
    Main controller for files manipulation
    """

    # ========================================================================
    #                             Constructor
    # ========================================================================
    def __init__(self, filename):
        """
        str -> FileManipulator

        filename - the complete filename path

        Class constructor
        """
        self.filename = filename
        self.log = None

    # ========================================================================
    #                            Public methods
    # ========================================================================
    def set_log(self, log):
        """
        LogManipulator -> None

        log - LogManipulator object

        Sets the log file
        """
        self.log = log

    def set_output_filename(self):
        """
        None -> str

        Builds a complete filename path and returns it
        """
        try:
            path_filename, extension = path.splitext(self.filename)
            num = 00
            _dir = listdir(path_filename[:path_filename.rindex('/') + 1])
            filename = path_filename[path_filename.rindex('/') + 1:]
            out = filename + '_filtrado_' + '%02d' % num + extension

            while True:
                if out in _dir:
                    num += 1
                    out = filename + '_filtrado_' + '%02d' % num + extension
                else:
                    return path_filename[:path_filename.rindex('/') + 1] + out

        except IOError:
            e = exc_info()[1]
            self.log.write('Erro! FileManipulator.set_output_filename\n' +
                           str(e))


class LogManipulator(FileManipulator):
    """
    Controller for log files manipulation
    """

    # ========================================================================
    #                             Constructor
    # ========================================================================
    def __init__(self, filename):
        """
        str -> LogManipulator

        filename - the complete filename path

        Class constructor
        """
        super(LogManipulator, self).__init__(filename)
        self._log = TxtFileIO(self.filename)
        self.__clear()
        self.set_log(self)
        self.console = sys.stdout

    # ========================================================================
    #                            Public methods
    # ========================================================================
    def write(self, line):
        """
        str -> None

        line - A line of text

        Writes a line to the log file
        """
        self.__open()
        self._log.write_line(self.__get_time() + ' - ' + line + '\n')
        self.close()

    def close(self):
        """
        None -> None

        Closes the stream and frees the file from memory
        """
        self._log.close()

    # ========================================================================
    #                            Private methods
    # ========================================================================
    def __open(self):
        """
        None -> bool

        Opens a log file
        """
        self._log.open('a')

    def __get_time(self):
        """
        None -> str

        Returns a formatted str of datetime.now()
        """
        return datetime.strftime(datetime.now(), '%d/%m/%Y - %H:%M:%S.%f')

    def __clear(self):
        """
        None -> None

        Clears the previous log
        """
        self._log.open('w')
        self._log.close()


class CsvManipulator(FileManipulator):
    """
    Controller for csv files manipulation
    """

    # ========================================================================
    #                             Constructor
    # ========================================================================
    def __init__(self, filename, log):
        """
        str -> CsvManipulator

        filename - the complete filename path
        log - LogManipulator object

        Class constructor
        """
        super(CsvManipulator, self).__init__(filename)
        self.titles = []
        self.data = []
        self.total_data = []
        self.row_len = 0
        self.set_log(log)

    # ========================================================================
    #                            Public methods
    # ========================================================================
    def csv_to_line_list(self, first_row, delimiter):
        """
        Bool, str -> Bool

        first_row - boolean to populate titles
        delimiter - csv file delimiter ';' or ','

        Reads csvfile and populate titles and data
        """
        try:
            with open(self.filename) as csvfile:
                spamreader = csv.reader(csvfile, delimiter=delimiter)
                for row in spamreader:
                    if first_row:
                        self.titles = row
                        first_row = False
                    else:
                        self.data.append(row)
                        if len(row) > self.row_len:
                            self.row_len = len(row)

            self.log.write("Listas de títulos e dados criadas a partir do csv")

            return True
        except IOError:
            e = exc_info()[1]
            self.log.write('Erro! CsvManipulator.csv_to_line_list\n' + str(e))
            return False

    def clean_csv(self, suffix):
        """
        str -> None

        suffix - the suffix of the filename (_temp.csv or _filtered.csv)

        Corrects titles and removes blank lines
        """
        if suffix == "_temp.csv":
            try:
                self.__correct_titles()
                self.log.write('Lista de títulos corrigida com sucesso')
            except IOError:
                e = exc_info()[1]
                self.log.write('Erro! CsvManipulator.__correct_titles\n' + str(e))

        try:
            self.__populate_total_data()
            self.log.write('Lista de dados total (títulos e dados) do ficheiro ' + suffix + ' criada com sucesso')
        except IOError:
            e = exc_info()[1]
            self.log.write('Erro! CsvManipulator.__populate_total_data\n' + str(e))

        try:
            self.__create_csv(suffix)
            self.log.write('Ficheiro ' + suffix + ' criado com sucesso')
        except IOError:
            e = exc_info()[1]
            self.log.write('Erro! CsvManipulator.__create_csv\n' + str(e))

        try:
            self.__remove_blank_lines(suffix)
            self.log.write('Linhas em branco removidas com sucesso do ficheiro ' + suffix)
        except IOError:
            e = exc_info()[1]
            self.log.write('Erro! CsvManipulator.__remove_blank_lines\n' + str(e))

    def write_csv(self):
        """
        None -> str

        Writes a new csv file filtered with the help of an sqlite database
        """
        csvfile = self.filename[self.filename.rfind("/") + 1:self.filename.rfind('.')] + '_temp.csv'

        db = CsvDB(self.filename[:self.filename.rfind("/") + 1], self.log, csvfile)

        db.create()
        self.data = db.filter()
        if len(self.data) > 0:
            self.filename = self.filename[:self.filename.rfind('.') + 1]
            db.destroy()

        self.clean_csv("_filtered.csv")

        self.filename = self.filename[:self.filename.rfind('.')] + '_filtered.csv'

        return self.filename

    # ========================================================================
    #                            Private methods
    # ========================================================================
    def __correct_titles(self):
        """
        None -> None

        Corrects titles so it doesn't have blank or repeated values
        """
        if len(self.titles) != self.row_len:
            dif = self.row_len - len(self.titles)
            for num in range(dif):
                self.titles.append(num)

        # remove spaces from the titles because of DB
        tmp_titles = []
        for title in self.titles:
            tmp_title = ''
            for char in title:
                if char == ' ':
                    tmp_title += '_'
                else:
                    tmp_title += char
            tmp_titles.append(tmp_title)
        self.titles = tmp_titles

        index = len(self.titles) - 1
        while index > 0:
            if self.titles[index] == self.titles[index - 1] or self.titles[index] == '':
                self.titles[index] = self.titles[index] + str(index)
            index -= 1

    def __populate_total_data(self):
        """
        None -> None

        Populates total_data with corrected titles and data
        """
        self.total_data = []
        self.total_data.append(self.titles)
        for row in self.data:
            if len(row) < self.row_len:
                diff = self.row_len - len(row)
                for num in range(diff):
                    row.append('')
            self.total_data.append(row)

    def __create_csv(self, suffix):
        """
        None -> None

        Writes a new csv file with corrected titles
        """
        with open(self.filename[:self.filename.rfind('.')] + suffix, 'w') as csvfile:
            spamwriter = csv.writer(csvfile)
            spamwriter.writerows(self.total_data)

    def __remove_blank_lines(self, suffix):
        """
        None -> None

        Removes blank lines from the temp csv
        """
        with open(self.filename[:self.filename.rfind('.')] + suffix, 'r') as filehandle:
            lines = filehandle.readlines()

        with open(self.filename[:self.filename.rfind('.')] + suffix, 'w') as filehandle:
            lines = filter(lambda x: x.strip(), lines)
            filehandle.writelines(lines)


class TxtManipulator(FileManipulator):
    """
    Controller for txt files manipulation
    """

    # ========================================================================
    #                             Constructor
    # ========================================================================
    def __init__(self, filename, log):
        """
        str -> TxtManipulator

        filename - the complete filename path
        log - LogManipulator object

        Class constructor
        """
        super(TxtManipulator, self).__init__(filename)
        self.line_list = []
        self.set_log(log)

    # ========================================================================
    #                            Public methods
    # ========================================================================
    def txt_to_line_list(self, extension):
        """
        None -> bool

        Opens a file, reads it and create a list containing another lists for
        each lines of the file. Returns a boolean flag accordingly
        """
        f = TxtFileIO(self.filename)
        try:
            f.open('r')
            self.line_list = f.read(extension)
            f.close()

            return True

        except IOError:
            e = exc_info()[1]
            self.log.write('Erro! TxtManipulator.txt_to_line_list\n' + str(e))
            f.close()

            return False

    def get_line_list(self):
        """
        None -> list

        Returns a line of text from the file
        """
        try:
            return self.line_list
        except IOError:
            e = exc_info()[1]
            self.log.write('Erro! TxtManipulator.get_line_list\n' + str(e))

    def set_line_list(self, line_list):
        """
        list -> None

        line_list - a list containing a line of text

        Sets the line list
        """
        self.line_list = line_list

    def clean_txt(self, selection, extension):
        """
        list -> None

        selection - a list of ints, representing the selection of filters to
                    apply in the line_list.
                    0 - don't apply filter
                    1 - apply filter
        extension - file extension (txt or csv)

        Applies a filter to line list accordingly to the selection list
        """
        if selection[0]:
            self.__remove_near_coords_lines()
        if selection[1]:
            self.__remove_NaN_lines()
        if selection[2]:
            self.__remove_blank_lines(extension)
        if selection[3]:
            self.__remove_duplicated_lines()

    def write_txt(self, set_new_filename=True):
        """
        None -> None

        set_new_filename - a bool flag to build or not a new filename

        Opens a file and writes to it a list containing each lines in lists.
        Chooses an appropriate name for the file created.
        """
        f = None
        try:
            if set_new_filename:
                output_filename = self.set_output_filename()
                f = TxtFileIO(output_filename)
                f.open('w')
                f.write(self.line_list)
            else:
                output_filename = self.filename
                f = TxtFileIO(output_filename)
                f.open('w')
                for line in self.line_list:
                    f.write_line(line)
            return output_filename[output_filename.rfind(sep) + 1:]
        except IOError:
            e = exc_info()[1]
            self.log.write('Erro! TxtManipulator.write_txt\n' + str(e))
        finally:
            if f:
                f.close()

    # ========================================================================
    #                            Private methods
    # ========================================================================
    def __remove_blank_lines(self, extension):
        """
        None -> None

        Remove the lists containing blank lines from the line list
        """
        try:
            temp_list = self.line_list[:]
            if extension == "txt":
                self.line_list = [i for i in temp_list if i[0][0].isnumeric()]
            else:
                self.line_list = [i for i in temp_list if i[0][4].isnumeric()]
        except IOError:
            e = exc_info()[1]
            self.log.write('Erro! TxtManipulator.__remove_blank_lines\n' +
                           str(e))

    def __remove_duplicated_lines(self):  # TODO: tinha a ver com tempo
        """
        None -> None

        Remove the duplicated lists from the line list
        """
        try:
            temp_list = self.line_list[:]
            self.line_list = []
            last_item = []

            for item in temp_list:
                if item != last_item:
                    self.line_list.append(item)
                    last_item = item
        except IOError:
            e = exc_info()[1]
            self.log.write('Erro! TxtManipulator.__remove_duplicated_lines\n' +
                           str(e))

    def __remove_NaN_lines(self):
        """
        None -> None

        Remove the lists containing lines with the word 'NaN' or
        'NaN (Não é um número)' from the line list
        """
        try:
            temp = self.line_list[:]
            self.line_list = [i for i in temp if "NaN" not in i[0]]
            temp = self.line_list[:]
            temp2 = [i for i in temp if "NaN (Não é um número)" not in i[0]]
            self.line_list = temp2
        except IOError:
            e = exc_info()[1]
            self.log.write('Erro! TxtManipulator.__remove_NaN_lines\n' +
                           str(e))

    def __remove_near_coords_lines(self):  # TODO: test
        """
        None -> None

        Remove the lists containing close coordinates lines from the line list
        """
        try:
            stopped = False
            moved = False
            previous_longitude = 0.0
            previous_latitude = 0.0
            temp_list = self.line_list[:]
            self.line_list = []

            for item in temp_list:
                if len(item) > 2:
                    longitude = float(item[5])
                    latitude = float(item[6].rstrip("\r\n"))
                    dif_long = abs(longitude - previous_longitude)
                    dif_lat = abs(latitude - previous_latitude)
                    coords_sum = (dif_long + dif_lat) / 2.0
                    if item[2] == "moved via ":
                        if stopped is True:
                            self.line_list.append(item)
                            stopped = False
                            moved = True
                            previous_longitude = longitude
                            previous_latitude = latitude
                        elif stopped is False and moved is True and \
                                coords_sum > 0.001:
                            self.line_list.append(item)
                            previous_longitude = longitude
                            previous_latitude = latitude
                        else:
                            # (stopped and moved) = False
                            self.line_list.append(item)
                            moved = True
                            previous_longitude = longitude
                            previous_latitude = latitude
                    elif item[2] == "stopped ":
                        if stopped is False:
                            self.line_list.append(item)
                            stopped = True
                            moved = False
                            previous_longitude = longitude
                            previous_latitude = latitude
                        elif stopped is True and coords_sum > 0.001:
                            self.line_list.append(item)
                            moved = False
                            previous_longitude = longitude
                            previous_latitude = latitude
        except IOError:
            e = exc_info()[1]
            self.log.write('Erro! TxtManipulator.__remove_coords_lines\n' +
                           str(e))


class XlsManipulator(FileManipulator):
    """
    Controller for Excel files manipulation
    """

    # ========================================================================
    #                             Constructor
    # ========================================================================
    def __init__(self, filename, log):
        """
        str -> XlsManipulator

        filename - the complete filename path
        log - LogManipulator object

        Class constructor
        """
        super(XlsManipulator, self).__init__(filename)
        self.set_log(log)
        self.chosen_worksheet = ''
        self.file = None  # file Object
        self.db = None
        self.headers = []
        self.headers_types = []
        self.chosen_col_index = 0
        self.xls_line = []
        self.num_dates_in_line = 0

    # ========================================================================
    #                            Public methods
    # ========================================================================
    def read(self):
        """
        None -> list

        Instantiates a ReadExcel object, opens it and returns a list containing
        the tabs names
        """
        try:
            self.file = ReadExcel(self.log)
            return self.file.open(self.filename)
        except IOError:
            e = exc_info()[1]
            self.file.close()
            self.log.write('Erro! XlsManipulator.read\n' + str(e))

    def choose_worksheet(self, worksheet_name=None):
        """
        str -> list

        worksheet_name - the name of the selected worksheet

        Sets the selected worksheet name to manipulate that worksheet
        """
        try:
            if worksheet_name:
                self.chosen_worksheet = worksheet_name
            else:
                self.chosen_worksheet = 'Final GE'

            return self.__get_columns_headers()
        except IOError:
            e = exc_info()[1]
            self.file.close()
            self.log.write('Erro! XlsManipulator.choose_worksheet\n' + str(e))

    def choose_reference_col(self, chosen_col_index=0):
        """
        int -> None

        chosen_col_index - the column index to filter

        Sets the reference column index
        """
        try:
            self.xls_line = self.file.get_next_row()
            if chosen_col_index:
                self.chosen_col_index = chosen_col_index
            else:
                current_index = -1
                for n in self.xls_line:
                    current_index += 1
                    if type(n) is tuple:
                        try:
                            # https://bytes.com/topic/python/answers/101490-create-datetime-instance-using-tuple
                            datetime(*n)
                            self.chosen_col_index = current_index
                        except IOError:
                            e = exc_info()[1]
                            self.file.close()
                            m1 = 'XlsManipulator.choose_reference_col\n'
                            m2 = 'Erro! Excel sem células de Datas\n' + str(e)
                            self.log.write('Erro!' + m1 + m2)
            if self.__format_datetime(False):
                self.__set_headers_types()
        except IOError:
            e = exc_info()[1]
            self.file.close()
            self.log.write('Erro! XlsManipulator.choose_reference_col\n' +
                           str(e))

    def xls_to_db(self):
        """
        None -> None

        Instantiates a sqlite database and fills it with every row of the
        selected worksheet
        """
        try:
            self.db = TempDB(self.filename[:self.filename.rindex('/') + 1],
                             self.log)
            self.db.create(self.headers, self.headers_types)
            self.db.insert(self.xls_line)
            n_rows = self.file.get_n_rows() - 2
            while n_rows:
                if self.__format_datetime():
                    self.db.insert(self.xls_line)
                n_rows -= 1
            self.db.commit()
        except IOError:
            self.db.close()
            e = exc_info()[1]
            m = 'XlsManipulator.xls_to_db\nErro ao inserir dados na DB\n'
            self.log.write(m + str(e))
        finally:
            self.file.close()

    def write(self, is_date_col=True):
        """
        None -> None

        Writes a filtered by date Excel file
        """
        try:
            formatted_xls = WriteExcel(self.log)

            if is_date_col:
                self.db.select("SELECT DISTINCT Data FROM t1")
                dates = []
                date_exists = True

                while date_exists:
                    _date = self.db.fetch_one()
                    if _date:
                        dates.append(_date)
                    else:
                        date_exists = False

                for _date in dates:
                    # _datetime_object = datetime.strptime(_date[0], '%d/%m/%Y')
                    _datetime_object = date.fromisoformat(_date[0][:10])
                    formatted_xls.add_sheet(self.__get_week_days(_datetime_object))
                    formatted_xls.write_row(self.headers)

                    query = "SELECT * FROM t1 WHERE Data = '" + _date[0][:10] + "'"
                    self.db.select(query)
                    row_exists = True

                    while row_exists:
                        row_values = self.db.fetch_one()

                        if row_values:
                            formatted_xls.write_row(row_values)
                        else:
                            row_exists = False
            else:
                col_name = self.headers[self.chosen_col_index]
                self.db.select(f"SELECT DISTINCT {col_name} FROM t1")
                values = []
                value_exists = True

                while value_exists:
                    _value = self.db.fetch_one()
                    if _value:
                        values.append(_value)
                    else:
                        value_exists = False

                for val in values:
                    formatted_xls.add_sheet(str(val[0]))
                    formatted_xls.write_row(self.headers)

                    query = f"SELECT * FROM t1 WHERE {col_name} = '" + str(val[0]) + "'"
                    self.db.select(query)
                    row_exists = True

                    while row_exists:
                        row_values = self.db.fetch_one()

                        if row_values:
                            formatted_xls.write_row(row_values)
                        else:
                            row_exists = False

            output_filename = self.set_output_filename()
            formatted_xls.save_workbook(output_filename)

            self.db.close()
            self.db.destroy()

            return output_filename[output_filename.rfind(sep) + 1:]

        except IOError:
            e = exc_info()[1]
            self.log.write('Erro! XlsManipulator.write\n' + str(e))

    # ========================================================================
    #                            Private methods
    # ========================================================================
    def __get_columns_headers(self):
        """
        None -> list

        Returns a list of the values in the first column of the chosen
        worksheet
        """
        try:
            self.file.open_worksheet(self.chosen_worksheet)
            self.headers = self.file.get_next_row()
            num = 1
            i = 0
            last_header = ''
            for header in self.headers:
                if not header:
                    new_header = last_header + str(num)
                    num += 1
                    self.headers[i] = new_header
                else:
                    last_header = header
                i += 1

            return self.headers
        except IOError:
            e = exc_info()[1]
            self.file.close()
            self.log.write('Erro! XlsManipulator.get_columns_headers\n' +
                           str(e))

    def __set_headers_types(self):
        """
        None -> None

        Creates a list of sqlite3 data types from the corresponding Excel data
        types
        """
        index = -1
        try:
            for n in self.xls_line:
                index += 1
                if type(n) is float and str(n)[-2:] != '.0':
                    self.headers_types.append('REAL')
                if type(n) is int or type(n) is float and str(n)[-2:] == '.0':
                    self.headers_types.append('INTEGER')
                if type(n) is str:
                    self.headers_types.append('TEXT')
                if type(n) is tuple or isinstance(n, datetime):
                    try:
                        datetime(*n)
                        self.num_dates_in_line += 1
                        self.headers_types.append('DATE')
                    except TypeError:
                        self.headers_types.append('BLOB')
                # else:
                #    self.headers_types.append('TEXT')
        except IOError:
            e = exc_info()[1]
            self.log.write('Erro! XlsManipulator.__set_headers_types\n' +
                           str(e))

    def __format_datetime(self, get_next_row=True):
        """
        bool -> bool

        get_next_row - boolean flag to get another row from the chosen Excel
                       worksheet

        Turns a datetime object into str and returns accordingly
        """
        try:
            description_index = self.headers.index('Description')
            if get_next_row:
                self.xls_line = self.file.get_next_row()
            try:
                if isinstance(self.xls_line[description_index], datetime):
                    _datetime_1 = self.xls_line[description_index]
                    _datetime_2 = self.xls_line[self.chosen_col_index]
                    str_time = _datetime_1.strftime("%H:%M:%S")
                    str_date = _datetime_2.strftime("%d/%m/%Y")
                    self.xls_line[description_index] = str_time
                    self.xls_line[self.chosen_col_index] = self.__get_week_days(_datetime_2)

                return True
            except IOError:
                return False
        except IOError:
            e = exc_info()[1]
            self.log.write('Erro! XlsManipulator.__format_datetime\n' + str(e))

    def __close_file(self):
        """
        None -> None

        Closes the stream and frees the file from memory
        """
        try:
            self.file.close()
        except IOError:
            e = exc_info()[1]
            self.log.write('Erro! XlsManipulator.__close_file\n' + str(e))

    def __get_week_days(self, _date):
        """
        datetime -> str

        _date - datetime object

        Returns a str representation of the date to be the name of the Excel
        Separator
        """
        week_day = ['2_', '3_', '4_', '5_', '6_', 'S_', 'D_']
        m = {1: 'JAN', 2: 'FEB', 3: 'MAR', 4: 'ABR', 5: 'MAI', 6: 'JUN',
             7: 'JUL', 8: 'AGO', 9: 'SET', 10: 'OUT', 11: 'NOV', 12: 'DEC'}
        '''
        date.weekday()
        Return the day of the week as an integer, where Monday is 0
        and Sunday is 6.
        For example, date(2002, 12, 4).weekday() == 2, a Wednesday
        '''
        try:
            return week_day[_date.weekday()] + str(_date.day) + m[_date.month]
        except IOError:
            e = exc_info()[1]
            self.log.write('Erro! XlsManipulator.__get_week_days\n' + str(e))


# #############################################################################
#                 PRESENTATION (VIEW) PACKAGE CLASSES
# #############################################################################

class Window(object):
    """
    Designs the Tk window interface
    """

    # ========================================================================
    #                             Constructor
    # ========================================================================
    def __init__(self, log):
        """
        FileIO -> Tk object

        Class constructor
        Designs the Tk window interface
        """
        # constants -----------------------------------------------------------
        self.master = Tk()
        self.master.title('Q - Filter 2.3')
        if platform != 'linux':
            self.master.iconbitmap(self.__set_icon())
            self.master.geometry("440x180")
        else:
            self.master.geometry("440x140")
        self.master.resizable(width=FALSE, height=FALSE)
        self.master.configure(borderwidth=1)
        self.log = log
        m1 = "Selecione filtro(s) antes de abrir um txt "
        m2 = "ou deixe em branco se abrir um xlsx ou csv"
        self.msg = m1 + m2
        self.bg = 'gray25'
        self.fg = 'white'
        self.ft = ('courier', 12, 'bold')

        # Variables -----------------------------------------------------------
        self.nans_var = IntVar(self.master)
        self.coords_var = IntVar(self.master)
        self.blanks_var = IntVar(self.master)
        self.repeated_var = IntVar(self.master)
        self.exec_file_var = IntVar(self.master)
        self.xls_tab_var = IntVar(self.master)
        self.xls_col_var = IntVar(self.master)
        self.is_read = False  # flag to know if the file was read
        self.file = None
        self.text_id = None  # the canvas message id
        self.selection = [0, 0, 0, 0]  # for the checkbuttons variables
        self.chosen_tab = StringVar(self.master)  # chosen xls tab
        self.chosen_col = StringVar(self.master)  # chosen excel date col
        self.userprefs = self.__get_user_prefs()  # prefs checkbuttons
        self.is_tab_chosen = False  # for the chosen excel tab
        self.is_col_chosen = False  # for the chosen excel date col

        # Draw Frames ---------------------------------------------------------
        self.frame1 = self.__draw_frame(self.master, 0, 2)
        self.frame2 = self.__draw_frame(self.master, 3, 3)

        # Draw Labels ---------------------------------------------------------
        self.__draw_label(self.frame2, "Log:", self.fg, 0, 1, stk=NW)
        self.__draw_label(self.frame1, "Filtros txt:", self.fg, 0, 2)
        self.__draw_label(self.frame2, "Boris & Vladimir Software",
                          "limegreen", 4, 3, font=("Quartz Ms", 9))

        # Draw Checkbuttons ---------------------------------------------------
        self.cb_NaNs = self.__draw_checkbutton(self.frame1, 'Nans',
                                               self.nans_var,
                                               self.__bind_chkbtn_NaNs, 2)
        self.cb_coords = self.__draw_checkbutton(self.frame1,
                                                 'Coordenadas próximas  ',
                                                 self.coords_var,
                                                 self.__bind_chkbtn_coords, 1)
        self.cb_blanks = self.__draw_checkbutton(self.frame1,
                                                 'Linhas em branco',
                                                 self.blanks_var,
                                                 self.__bind_chkbtn_blanks, 3)
        self.cb_repeated = self.__draw_checkbutton(self.frame1,
                                                   'Linhas repetidas',
                                                   self.repeated_var,
                                                   self.__bind_chkbtn_reps, 4)

        # Draw Buttons --------------------------------------------------------
        self.__draw_button(self.master, 'Abrir...', self.__open_file, 0, W)
        self.__draw_button(self.master, 'Gravar', self.__save_file, 1, W)
        self.__draw_button(self.master, 'Sair', self.__quit, 5, E)

        # Draw Canvas to display the program messages -------------------------
        self.canvas = self.__draw_canvas()

        # Put the first message to canvas -------------------------------------
        self.__set_message(self.msg)

        # Draw Menu and SubMenus ----------------------------------------------
        self.menu = Menu(self.master)
        self.master.config(menu=self.menu)
        self.__draw_sub_menu('Ficheiro', ['Abrir...', 'Gravar', 'Sair'],
                             [self.__open_file, self.__save_file, self.__quit])
        pref_labels = ['Abrir ficheiro após filtragem',
                       'Escolher manualmente o separador Excel',
                       'Escolher manualmente a coluna de data de referência',
                       'Guardar Preferências']
        pref_commands = [self.__bind_chkbtn_exec, self.__bind_chkbtn_xls_sep,
                         self.__bind_chkbtn_xls_col, self.__set_user_prefs]
        self.__draw_sub_menu('Preferências', pref_labels, pref_commands,
                             n_checkbuttons=3)
        self.__draw_sub_menu('Ajuda', ['Sobre', 'Ver log', 'Manual'],
                             [self.__show_about,
                              self.__show_log,
                              self.__show_man])

        # Window Mainloop -----------------------------------------------------
        self.master.mainloop()

    # ========================================================================
    #                            Private methods
    # ========================================================================
    # --------------------------- draw methods -------------------------------
    def __set_message(self, message):
        """
        str -> None

        message - the message to display on canvas

        Draws a message in the window canvas
        """
        if self.text_id:
            self.canvas.delete(self.text_id)
        if platform == 'linux':
            self.text_id = self.canvas.create_text(3, 3, anchor=NW,
                                                   text=message, width=255,
                                                   fill=self.fg)
        else:
            self.text_id = self.canvas.create_text(3, 3, anchor=NW,
                                                   text=message, width=267,
                                                   fill=self.fg)

    def __draw_frame(self, window, column, columnspan):
        """
        Tk, int, int -> Frame

        window - the Tk window in witch the Frame is draw
        column - the grid column in witch the Frame is draw
        columnspan - the number of columns in witch the frame is spanned

        Draws a Frame in the window
        """
        frame = Frame(window, borderwidth=2, bg=self.bg)
        frame.grid(row=0, column=column, rowspan=5, columnspan=columnspan,
                   padx=1, pady=1)

        return frame

    def __draw_label(self, f, text, fg, row, colspan, stk=None, font=None):
        """
        Frame, str, str, int, NW, tuple -> None

        f - a Frame object to draw the Label
        text - the text for the Label
        fg - the foreground color of the Label
        row - the grid row in witch the Label is designed
        colspan - the number of columns in witch the Label is spanned
        stk - the positional anchor to display the text of the Label
        font - the font type for the text of the Label

        Draws a Label in a Frame
        """
        if font:
            label = Label(f, text=text, font=font, bg=self.bg, fg=fg)
        else:
            label = Label(f, text=text, font=self.ft, bg=self.bg, fg=fg)

        if stk:
            label.grid(row=row, column=0, sticky=stk)
        else:
            label.grid(row=row, column=0, columnspan=colspan)

    def __draw_checkbutton(self, f, text, variable, command, row):
        """
        Frame str, IntVar, function, int -> Checkbutton

        f - the frame in witch the Checkbutton will be draw
        text - the text of the Checkbutton
        variable - the IntVar associated with the Checkbutton
        command - the method to execute when the Checkbutton is selected
        row - the grid row in witch the Checkbutton is designed

        Draws a Checkbutton in a Frame
        """
        checkbutton = Checkbutton(f, text=text, bg=self.bg,
                                  fg=self.fg, selectcolor='black',
                                  variable=variable, command=command)
        checkbutton.grid(row=row, column=0, columnspan=2, sticky=W)

        return checkbutton

    def __draw_radiobutton(self, f, text, variable):
        """
        Frame, str -> None

        f - the Frame where the radiobutton will be draw
        text - the text and value of the radiobutton
        variable - variable to bind the radiobuttons

        Draws a Radiobutton in a Frame
        """
        radiobutton = Radiobutton(f, text=text, variable=variable, value=text,
                                  indicatoron=0)
        radiobutton.grid(column=0, columnspan=2, sticky=EW)

    def __draw_button(self, window, text, command, column, sticky):
        """
        Tk window, str, function, int, W or E -> None

        window - the Tk Window in witch the button will be draw
        text - the text of the Button
        command - the method to execute when the Button is pressed
        column - the grid column in witch the Button is designed
        sticky - the positional anchor to display the Button on the Tk Window

        Draws a Button on the Tk Window
        """
        if platform == 'linux':
            button = Button(window, width=7, text=text, command=command)
        else:
            button = Button(window, width=10, text=text, command=command)
        button.grid(row=5, column=column, sticky=sticky, pady=2)

    def __draw_canvas(self):
        """
        None -> Canvas

        Draws a Canvas in a Frame
        """
        if platform == 'linux':
            canvas = Canvas(self.frame2, width=255, height=60)
        else:
            canvas = Canvas(self.frame2, width=267, height=75)
        canvas.config(bg='black')
        canvas.grid(row=1, rowspan=3, column=0, columnspan=3)

        return canvas

    def __draw_sub_menu(self, label, labels, commands, n_checkbuttons=0):
        """
        str, list, list, int -> None

        label - name to show in Menu
        labels - names to show in commands and / or checkbuttons
        commands - the commands to bind
        n_checkbuttons - number of checkbuttons to draw

        Draws the submenus in Menu
        """
        menu = Menu(self.menu)
        self.menu.add_cascade(label=label, menu=menu)

        if n_checkbuttons:
            c_labels = [labels[i] for i in range(n_checkbuttons)]
            labels = labels[n_checkbuttons:]
            c_commands = [commands[i] for i in range(n_checkbuttons)]
            commands = commands[n_checkbuttons:]
            variables = [self.exec_file_var, self.xls_tab_var,
                         self.xls_col_var]

            for i in range(len(c_labels)):
                menu.add_checkbutton(label=c_labels[i],
                                     command=c_commands[i],
                                     variable=variables[i])
        for i in range(len(labels)):
            menu.add_command(label=labels[i], command=commands[i])

    def __draw_toplevel(self, title):
        """
        str -> Toplevel

        title - the title of the Toplevel Window

        Draws a Toplevel Tk Window
        """
        toplevel = Toplevel(self.master)
        toplevel.configure(borderwidth=5)
        toplevel.title(title)
        if platform != 'linux':
            toplevel.iconbitmap(self.__set_icon())
        toplevel.resizable(width=FALSE, height=FALSE)

        return toplevel

    # ---------------------- event biding methods -----------------------------
    def __bind_chkbtn_coords(self):
        """
        None -> None

        Binds the IntVar associated with the Checkbutton to the selection list
        property
        """
        if self.selection[0] == 0:
            self.selection[0] = 1
            self.log.write('Filtro por coordenadas selecionado')
        else:
            self.selection[0] = 0
            self.log.write('Filtro por coordenadas selecionado')

    def __bind_chkbtn_NaNs(self):
        """
        None -> None

        Binds the IntVar associated with the Checkbutton to the selection list
        property
        """
        if self.selection[1] == 0:
            self.selection[1] = 1
            self.log.write('Filtro por NaNs selecionado')
        else:
            self.selection[1] = 0
            self.log.write('Filtro por NaNs desselecionado')

    def __bind_chkbtn_blanks(self):
        """
        None -> None

        Binds the IntVar associated with the Checkbutton to the selection list
        property
        """
        if self.selection[2] == 0:
            self.selection[2] = 1
            self.log.write('Filtro por linhas em branco selecionado')
        else:
            self.selection[2] = 0
            self.log.write('Filtro por linhas em branco desselecionado')

    def __bind_chkbtn_reps(self):
        """
        None -> None

        Binds the IntVar associated with the Checkbutton to the selection list
        property
        """
        if self.selection[3] == 0:
            self.selection[3] = 1
            self.log.write('Filtro por linhas repetidas selecionado')
        else:
            self.selection[3] = 0
            self.log.write('Filtro por linhas repetidas desselecionado')

    def __bind_chkbtn_exec(self):
        """
        None -> None

        Binds the IntVar associated with the Checkbutton (execute file)
        """
        if self.userprefs[0] == 0:
            self.userprefs[0] = 1
            self.exec_file_var.set(value=1)
            self.log.write('Abrir ficheiro após filtragem selecionado')
        else:
            self.userprefs[0] = 0
            self.exec_file_var.set(value=0)
            self.log.write('Abrir ficheiro após filtragem desselecionado')

    def __bind_chkbtn_xls_sep(self):
        """
        None -> None

        Binds the IntVar associated with the Checkbutton (show excel tabs) to
        the __show_excel_tabs method
        """
        if self.userprefs[1] == 0:
            self.userprefs[1] = 1
            self.xls_tab_var.set(value=1)
            self.log.write('Escolher separador Excel selecionado')
        else:
            self.userprefs[1] = 0
            self.xls_tab_var.set(value=0)
            self.log.write('Escolher separador Excel desselecionado')

    def __bind_chkbtn_xls_col(self):
        """
        None -> none

        Binds the IntVar associated with the Checkbutton (show excel headers)
        to the __show_excel_columns method
        """
        if self.userprefs[2] == 0:
            self.userprefs[2] = 1
            self.xls_col_var.set(value=1)
            self.log.write('Escolher coluna de Excel selecionado')
        else:
            self.userprefs[2] = 0
            self.xls_col_var.set(value=0)
            self.log.write('Escolher coluna de Excel desselecionado')

    def __open_file(self):
        """
        None -> None

        Binds the 'Open File...' Button to a filedialog, then calls the
        accordingly file manipulator
        """
        msg = 'Ficheiro a filtrar \ limpar...'
        self.log.write('Clique no botão "Abrir"')
        filepath = filedialog.askopenfilename(title=msg)
        filename = filepath[filepath.rfind(sep) + 1:]
        self.log.write('ficheiro ' + filename + ' escolhido')

        if filepath != () and filepath != '':
            extension = path.splitext(filepath)[1]

            # ------------------------ txt part -------------------------------
            if extension == '.txt':
                try:
                    self.file = TxtManipulator(filepath, self.log)
                    self.log.write(filename + ' carregado com sucesso')
                    self.__set_message('Ficheiro carregado com sucesso')

                    self.is_read = self.file.txt_to_line_list("txt")
                    self.log.write(filename + ' - Filtros selecionados')
                    self.__set_message('A aplicar filtro(s)...')

                    self.file.clean_txt(self.selection, "txt")
                    self.log.write(filename + ' - filtrado com sucesso')
                    m = 'Ficheiro filtrado \ limpo com sucesso\nClique Gravar'
                    self.__set_message(m)

                except IOError:
                    e = exc_info()[1]
                    self.__set_message('Erro ao ler ficheiro!\n %s' % str(e))
                    self.log.write('Window.__open_file\n' + str(e))
                    self.log.close()

            # ------------------------ csv part -------------------------------
            if extension == '.csv':
                try:
                    self.file = CsvManipulator(filepath, self.log)
                    self.log.write(filename + ' carregado com sucesso')
                    self.__set_message('Ficheiro carregado com sucesso')

                    self.is_read = self.file.csv_to_line_list(True, ";")
                    self.log.write(filename + ' - Carregada com sucesso para a memória')
                    self.__set_message('A aplicar filtro(s)...')

                    self.file.clean_csv("_temp.csv")
                    self.log.write(filename + ' - filtrado com sucesso')
                    m = 'Ficheiro filtrado \ limpo com sucesso\nClique Gravar'
                    self.__set_message(m)

                except IOError:
                    e = exc_info()[1]
                    self.__set_message('Erro ao ler ficheiro!\n %s' % str(e))
                    self.log.write('Window.__open_file\n' + str(e))
                    self.log.close()

            # ------------------------ xls part -------------------------------
            elif extension == '.xlsx':
                try:
                    self.file = XlsManipulator(filepath, self.log)
                    worksheet_names = self.file.read()

                    if self.xls_tab_var.get():
                        self.is_tab_chosen = False
                        self.__show_excel_tabs(worksheet_names, filename)

                    elif self.xls_col_var.get():
                        headers = self.file.choose_worksheet()
                        self.is_col_chosen = False
                        self.__show_excel_columns(headers, filename)
                    else:
                        self.file.choose_reference_col()
                        self.__xls_to_db(filename)
                        self.is_read = True

                except IOError:
                    e = exc_info()[1]
                    self.__set_message('Erro ao ler ficheiro!\n %s' % str(e))
                    self.log.write('Window.__open_file\n' + str(e))
                    self.log.close()

            # ------------------- other extensions part -----------------------
            else:
                m = 'O ficheiro escolhido não tem um formato válido.\n' + \
                    'Escolha um txt, um csv ou um xlsx'
                self.log.write(filename + ' Escolha errada de ficheiro')
                self.__set_message(m)

    def __save_file(self):
        """
        None -> None

        Binds the 'Save' Button to save the manipulated file
        """
        if self.is_read:
            try:
                # -------------- txt part -------------------------------------
                if type(self.file) is TxtManipulator:
                    file = self.file.write_txt()
                    self.log.write(file + ' - gravado com sucesso')
                    self.__reset_window()
                    if self.exec_file_var.get():
                        try:
                            if platform == 'linux':
                                o_open(file, O_RDONLY)
                            else:
                                startfile(file)
                        except EnvironmentError:
                            if platform == 'linux':
                                o_open(self.__get_dir_path() + sep + file,
                                       O_RDONLY)
                            else:
                                startfile(self.__get_dir_path() + sep + file)

                # ------------------ csv part ---------------------------------
                elif type(self.file) is CsvManipulator:
                    file = self.file.write_csv()
                    self.log.write(file + ' gravado com sucesso')
                    self.__reset_window()
                    if self.exec_file_var.get():
                        try:
                            if platform == 'linux':
                                o_open(file, O_RDONLY)
                            else:
                                startfile(file)
                        except EnvironmentError:
                            if platform == 'linux':
                                o_open(self.__get_dir_path() + sep + file,
                                       O_RDONLY)
                            else:
                                startfile(self.__get_dir_path() + sep + file)

                # -------------- xls part -------------------------------------
                elif type(self.file) is XlsManipulator:
                    if self.chosen_col.get() == 'Data':
                        file = self.file.write()
                    else:
                        file = self.file.write(is_date_col=False)
                    self.log.write(file + ' gravado com sucesso')
                    self.__reset_window()
                    if self.exec_file_var.get():
                        try:
                            if platform == 'linux':
                                o_open(file, O_RDONLY)
                            else:
                                startfile(file)
                        except EnvironmentError:
                            if platform == 'linux':
                                o_open(self.__get_dir_path() + sep + file,
                                       O_RDONLY)
                            else:
                                startfile(self.__get_dir_path() + sep + file)

                # ------------ others part ------------------------------------
                else:
                    m = 'Antes de gravar tem de abrir um ficheiro txt, csv ou xlsx\n'
                    self.log.write('Clique no botão "Gravar" sem antes abrir')
                    self.__set_message(m + self.msg)
            except WindowsError:
                e = exc_info()[1]
                self.__set_message('Erro ao gravar ficheiro!\n %s' % str(e))
                self.log.write('Window.__save_file\n' + str(e))
                self.log.close()

    def __quit(self):
        """
        None -> None

        Binds the 'Quit' Button to destroy the Tk Window
        """
        self.master.destroy()
        self.log.close()

    def __show_about(self):
        """
        None -> None

        Associated with the Help Menu.
        Creates a new window with the "About" information
        """
        appversion = "2.3 "
        app = " Q-Filter"
        copyright = '(c) 2014' + 2 * ' ' + \
                    'SDATO - DP - UAF - GNR'
        txt0 = '''
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
               '''
        lic = 'https://www.gnu.org/licenses/gpl-3.0.txt'
        contactname = "Nuno Venâncio"
        contactphone = "(00351) 969 564 906"
        contactemail = "venancio.gnr@gmail.com\n"
        txt1 = "Version: " + appversion + 2 * "\n"
        spc = 9 * ' '
        txt2 = spc + "Copyleft: " + copyright + "\n" + spc + "Licença: " + lic
        txt3 = contactname + '\n' + contactphone + '\n' + contactemail
        fg = 'white'
        font1 = ('courier', 20, 'bold')
        font2 = ('courier', 9)

        toplevel = self.__draw_toplevel('Sobre...')
        frame = self.__draw_frame(toplevel, 0, 2)
        self.__draw_label(frame, app, fg, 0, 2, stk=W, font=font1)
        self.__draw_label(frame, txt1, fg, 1, 1, stk=E)
        self.__draw_label(frame, txt2, fg, 2, 1, stk=W, font=font2)
        self.__draw_label(frame, txt0, fg, 3, 1, stk=W, font=font2)
        self.__draw_label(frame, txt3, fg, 4, 3, font=font2)
        self.__draw_button(toplevel, 'Ok', toplevel.destroy, 1, W)

    def __show_excel_tabs(self, tabs, filename):
        """
        list, str -> None

        tabs - a list containing the tab names
        filename - the complete filename path

        Associated with the Preferences Menu
        Creates a new window with checkbuttons corresponding to the excel
        tabs, so the user can select the tab in witch the program should work
        """
        fg = 'white'
        toplevel = self.__draw_toplevel('Separadores')
        frame = self.__draw_frame(toplevel, 0, 2)
        self.__draw_label(frame, "Selecione o separador Excel", fg, 0, 2,
                          font=self.ft)

        def destroy():
            if self.chosen_tab.get():
                self.is_tab_chosen = True
                message = 'Tab ' + self.chosen_tab.get() + ' selecionada'
                self.log.write(message)
                self.__set_message(message)

                worksheet_name = self.chosen_tab.get()
                headers = self.file.choose_worksheet(worksheet_name)
                self.chosen_tab.set('')

                if self.xls_col_var.get():
                    toplevel.destroy()
                    self.__show_excel_columns(headers, filename)
                else:
                    toplevel.destroy()
                    self.file.choose_reference_col()
                    self.__xls_to_db(filename)

            else:
                self.log.write('Nenhuma tab excel escolhida')
                self.__set_message('Escolha uma tab Excel')

        for i in range(len(tabs)):
            self.__draw_radiobutton(frame, tabs[i], self.chosen_tab)

        self.__draw_button(toplevel, 'Ok', destroy, 1, W)

    def __show_excel_columns(self, cols, filename):
        """
        list, str -> None

        cols - a list containing the excel headers
        filename - the complete filename path

        Associated with the Preferences Menu
        Creates a new window with checkbuttons corresponding to the excel
        columns, so the user can select the one in witch the program
        should work
        """
        fg = 'white'
        toplevel = self.__draw_toplevel('Colunas')
        frame = self.__draw_frame(toplevel, 0, 2)
        self.__draw_label(frame, "Selecione a coluna de referência",
                          fg, 0, 2, font=self.ft)

        def destroy():
            if self.chosen_col.get():
                self.is_col_chosen = True
                message = 'Coluna ' + self.chosen_col.get() + ' escolhida'
                self.log.write(message)
                self.__set_message(message)
                index = cols.index(self.chosen_col.get())
                self.chosen_col.set('')
                self.file.choose_reference_col(index)
                toplevel.destroy()
                self.__xls_to_db(filename)

        for i in range(len(cols)):
            self.__draw_radiobutton(frame, cols[i], self.chosen_col)

        self.__draw_button(toplevel, 'Ok', destroy, 1, W)

    def __show_log(self):
        """
        None -> None

        Opens the log file
        """
        if platform == 'linux':
            o_open(self.__get_dir_path() + sep + "log.log", O_RDONLY)
        else:
            startfile(self.__get_dir_path() + sep + "log.log")

    def __show_man(self):
        """
        None -> None

        Opens manual file
        """
        if platform == 'linux':
            o_open(self.__get_dir_path() + sep + "manual.pdf", O_RDONLY)
        else:
            startfile(self.__get_dir_path() + sep + "manual.pdf")

    # -------------------------- other helper methods -------------------------
    def __get_user_prefs(self):
        """
        None - list

        Returns a list of boolean ints corresponding to the user preferences
        """
        filename = self.__get_dir_path() + sep + 'userprefs'
        try:
            userprefs = TxtManipulator(filename, self.log)
            line_list = [0, 0, 0]
            if userprefs.txt_to_line_list("txt") or userprefs.txt_to_line_list("csv"):
                line_l = userprefs.get_line_list()
                line_list = [int(line[0][0]) for line in line_l]

            self.exec_file_var.set(value=line_list[0])
            self.xls_tab_var.set(value=line_list[1])
            self.xls_col_var.set(value=line_list[2])
            self.log.write('Preferências carregadas com sucesso')

            return line_list

        except IOError:
            e = exc_info()[1]
            self.__set_message('Erro ao ler ficheiro de preferêcias!\n %s' %
                               str(e))
            self.log.write('Window.__get_user_prefs\n' + str(e))

    def __set_user_prefs(self):
        """
        None -> None

        Saves the user preferences to the userprefs file
        """
        filename = self.__get_dir_path() + sep + 'userprefs'

        try:
            userprefs = TxtManipulator(filename, self.log)
            line_list = []

            for i in self.userprefs:
                line_list.append(str(i) + '\n')
            userprefs.set_line_list(line_list)
            userprefs.write_txt(set_new_filename=False)
            self.__set_message('Preferências gravadas com sucesso')
            self.log.write('Preferências alteradas com sucesso')
            self.userprefs = self.__get_user_prefs()
        except IOError:
            e = exc_info()[1]
            self.__set_message('Erro ao gravar ficheiro de preferêcias!\n %s' %
                               str(e))
            self.log.write('Window.__set_user_prefs\n' + str(e))

    def __xls_to_db(self, filename):
        """
        str -> None

        filename - the complete filename path

        Turns the excel values in db values
        """
        self.file.xls_to_db()
        self.is_read = True
        m = 'Ficheiro filtrado com sucesso\nClique Gravar'
        self.log.write(filename + ' filtrado com sucesso')
        self.__set_message(m)

    def __reset_window(self):
        """
        None -> None

        Resets the Tk Window, file and selection fields, IntVars associated
        with the Checkbuttons, and sets a new message
        """

        self.is_read = False
        self.cb_NaNs.deselect()
        self.cb_coords.deselect()
        self.cb_blanks.deselect()
        self.cb_repeated.deselect()
        self.file = None
        self.selection = [0, 0, 0, 0]
        self.__set_message('Ficheiro gravado com sucesso\n' + self.msg)

    def __set_icon(self):
        """
        None -> str

        Returns the path of the icon file
        """
        return self.__get_dir_path() + sep + "Q.ico"

    def __get_dir_path(self):
        """
        None -> str

        Returns the path of the working directory
        """
        return path.dirname(path.realpath(__file__))


# #############################################################################
#                                 START
# #############################################################################

if __name__ == '__main__':
    stdout = getwriter('utf8')(stdout)
    dir_path = path.dirname(path.realpath(__file__))
    log = LogManipulator(dir_path + sep + "log.log")
    sys.stderr = log
    log.write('Inicio do programa')
    Window(log)
